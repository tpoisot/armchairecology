---
title: "Against Notebooks"
slug: against-notebooks
date: 2019-11-09
draft: false
topics: [notebooks, replicability, teaching]
author: tpoisot
---

This title might be a bit more provocative than intended, but hear me out: after
a year of using Jupyter notebooks weekly for coding, thinking, and teaching, I
like them less and less. [Joel
Gru](https://towardsdatascience.com/the-case-against-the-jupyter-notebook-d4da17e97243)
*et al.* gave a bunch of arguments against notebooks, which while technically
true, are a little bit nitpicky and not that relevant when interacting with
beginners. My own complaints are a little bit different: notebooks fall short on
two elements, **text editing** and **portability**.

Text editing is the part of notebooks that frustrate me the most; for something
designed to convey technical information, the text editor is hostile. Markdown
is supported, but there is no way to add references (besides plugins who will
work, or won't). There is no way to cite anything, and so the text part of the
notebook is either context free, or just a pain of manually curating the
literature cited.

This makes notebooks a very poor format for lessons. There are clunky
workarounds, of course. One can write the document as a markdown with code, then
compile this with `pandoc` to get the references formatted, then convert again
to a notebook format. This works, but results in a read-only notebook. That's
bad.

For a format built on `JSON`, it is baffling not to see citation support.
Citeproc-json is a perfectly fine citation format, which can even be embedded in
the notebook itself. As far as I'm concerned, lack of support for citations is
the number one hindrance to adopting notebooks for teaching seriously.

Coming back to the issue of plugins: it's bad. Plugins are installed on the
server, and so sharing a notebook to someone with different plugins will not
work. We use *JuliaBox* in the classroom, and they ship packages that are not
supported on *Binder*, or not available on our own server, and so there is a
loss of functionality associated to using the same document across different
locations.

In short, doing anything more advanced with a notebook than a bit of code and a
bit of text is a pain, and this is not encouraging to move towards a
notebook-based framework.
