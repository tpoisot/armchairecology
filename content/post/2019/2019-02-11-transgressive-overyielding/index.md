---
title: Transgressive overyielding and coexistence
subtitle: Using isoclines to develop a visual intuition
date: 2019-02-11
slug: transgressive-overyielding
topics: [population dynamics, ecological modelling, teaching, transgressive overyielding, community ecology]
categories: [Quantified opinions]
uses: [d3]
---

One of my favorite topic when I discuss models with multiple variables is the
notion of transgressive overyielding. When two species are in competition, there
is a range of situations for which the total biomass when both species are
present is superior to what the most productive species would achieve alone. I
like this example not only because it has important implications for the
biodiveristy-ecosystem functioning relationship, but because it is a good
opportunity to develop intitutions on the notoriously difficult notion of
isoclines, as it lends itself to an easy visual interpretation.

And then, as I was writing this blog post, @PillGouh19 published a paper that
contains some provocative ideas about transgressive overyielding and its
relationship with the mechanisms of coexistence. Because I have not yet
processed the paper, much less the maths in it, I will not go into much details
-- but this is interesting to see that the discussion over transgressive
overyielding, coexistence, stability, and competition, is apparently not over.

The usual way to study this question is to use a two-species logistic model, as for example in @Lore04:

$$\dot N_1 = r_1\frac{N_1}{K_1}\left(K_1 - N_1 - \alpha_2N_2\right)$$
$$\dot N_2 = r_2\frac{N_2}{K_2}\left(K_2 - N_2 - \alpha_1N_1\right)$$

The competition strength is represented by the values of α, where $\alpha_1$ is
the effect of species 1 on species 2, and conversely.  So far, so good.

An isocline is the combinations of values of $N_x$ for which the growth of $x$
is null, or in other words it is the solution to $\dot N_x = 0$. Due to the way
the equations are written, we can solve this *per capita*, so that we divide
each $\dot N_i$ by $N_i$ and work on what is left. Isoclines are usually
represented on the same phase plot, so we will directly express them as $N_2 =
f(N_1)$. There will be two such functions: $f_1$ (derived from the expression of
$\dot N_1$), and $f_2$. Because we want the derivative to be 0, and both $r$ and
$K$ can be assumed to be positive, we can focus on the content of the
parenthesis. This yields

$$f_1(N_1) = -\frac{1}{\alpha_2}N_1+\frac{K_1}{\alpha_2}$$
$$f_2(N_1) = -\alpha_1N_1+{K_2}$$

The relevant information is that the isocline of a population is a linear
function of the density of the other population; its slope is the ration between
limitation *by* the other species and self-limitation, and its intercept is the
carrying capacity of the species *alone*. If the two isoclines intersect, there
is an equilibrium where both species coexist. Well, it's not quite true -- we
want the two lines to intersect at a point where $N_1^\star$ and $N_2^\star$ are
both larger than 0.

Notice something cool? The isoclines depend *only* of the carrying capacity and
the inter-specific competition strength. Neat, right? In other formulations of
the model, specifically those using both intra and inter-specific competition,
the growth rate remains here. The results are the same, but we have to deal with
a lot more parameters at every step, so this simplication makes sense (and there
is, in fact, one more simplication coming). In any case, the value of $K$
control the *intercepts*, and the values of $\alpha$ control the *slopes*. So by
picking the right combination of these parameters, we can make the lines cross
(or not). By the way, we are only interested in the lines crossing for $N_1$ and
$N_2$ greater than 0 -- there are many situations in which the lines cross but
where the equilibrium is not biologically meaningful.

 There is a nice trick to find when the equilibrium is stable, *i.e.* $\alpha_1
 < K_2/K_1 < 1/\alpha _2$ [@Case00]. Something familiar? The limit terms,
 $\alpha_1$ and $1/\alpha_2$, are the slopes of the isoclines. It's nice how
 everything fits together, isn't it?

You can play with the following figure to figure out conditions in which the
equilibrium exists. The isocline for the first population is in green, and for
the second population in brown. The solid circles on each axes are the values of
$K_1$ and $K_2$. The dot at the intersection between the two isoclines will turn
solid when the equilibrium is stable, and white when it is unstable.

{{< includehtml "interactive/isoclines1/layout.html" >}}
{{< includescript "interactive/isoclines1/script.js" >}}

There is something interesting about the situations wherein the equilibrium is
stable: it happens, in terms of isoclines, when $f_1(0) > f_2(0)$, *i.e.* the
intercept of the first isolcine on the axis for the second population is *above*
the carrying capacity of the second population. What this means is that the
equilibrium becomes stable when it is *above* an imaginary line going from $(0,
K_2)$ to $(K_1, 0)$:

{{< includehtml "interactive/isoclines2/layout.html" >}}
{{< includescript "interactive/isoclines2/script.js" >}}

What happens across this line, the *Relative Yield Total* (or RYT) is that we
*trade* units of $N_2$ and $N_1$ at a rate of $-K_2/K_1$, so that at any point,
the value of this line is $N_1/K_1+N_2/K_2$. When the equilibrium is *below*
this line, it is visually obvious that $N_1^\star + N_2^\star$ is *lower* than
best performing monoculture. But when the equilibrium is *above* the line, then
the stably coexisting two-species mix is outperforming the best monoculture, and
transgressive overyielding occurs.

Of course, it is also possible to work this out directly from the model, but I
think that the visual intuition of isoclines (although they are limited to
two-species models) is important. The whole analysis, including models with
non-linear isoclines, is done in @Lore04, where there are situations in which
coexistence and transgressive overyielding are distinct.
