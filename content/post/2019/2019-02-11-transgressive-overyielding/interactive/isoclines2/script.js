var pad = 50;
var container2 = document.getElementById("isocline2a");

// 2. Use the margin convention practice
var margin = {top: pad, right: pad, bottom: pad, left: pad}
  , width2 = container2.offsetWidth - margin.left - margin.right // Use the window's width
  , height2 = container2.offsetHeight - margin.top - margin.bottom; // Use the window's height

// 5. X scale will use the index of our data
var xScale2 = d3.scaleLinear()
    .domain([0, 2]) // input
    .range([0, width2]); // output

// 6. Y scale will use the randomly generate number
var yScale2 = d3.scaleLinear()
    .domain([0, 2]) // input
    .range([height2, 0]); // output

// 7. d3's line generator
var line2 = d3.line()
    .x(function(d) { return xScale2(d.x); }) // set the x values for the line generator
    .y(function(d) { return yScale2(d.y); }) // set the y values for the line generator

function updateData2 (fig, k1, k2, a1, a2) {
  var x = k2/k1;
  var n1star = k1*(1-x*a2)/(1-a2*a1);
  var n2star = k1*(x-a1)/(1-a2*a1);
  iso1 = [{"x": 0.0, "y": k1/a2}, {"x": k1, "y": 0.0}]
  iso2 = [{"x": 0.0, "y": k2}, {"x": k2/a1, "y": 0.0}]
  ryt = [{"x": 0.0, "y": k2}, {"x": k1, "y": 0.0}]
  fig.append("path")
      .datum(ryt) // 10. Binds data to the line
      .attr("class", "line") // Assign a class for styling
      .attr("d", line2)
      .attr("stroke", "rgb(200, 200, 200)")
      .attr("stroke-dasharray", "2px");
  fig.append("path")
      .datum(iso1) // 10. Binds data to the line
      .attr("class", "line") // Assign a class for styling
      .attr("d", line2)
      .attr("stroke", "rgb(116, 140, 105)"); // 11. Calls the line generator
  fig.append("path")
      .datum(iso2) // 10. Binds data to the line
      .attr("class", "line") // Assign a class for styling
      .attr("d", line2)
      .attr("stroke", "rgb(185, 113, 79)")
      .attr("stroke-dasharray", "5px");
  fig.append("circle")
      .datum({"x": 0.0, "y": k2})
      .attr("class", "dot")
      .attr("cx", function(d) { return xScale2(d.x) })
      .attr("cy", function(d) { return yScale2(d.y) })
      .attr("r", 5)
      .attr("fill", "rgb(185, 113, 79)");
  fig.append("circle")
      .datum({"x": k1, "y": 0.0})
      .attr("class", "dot")
      .attr("cx", function(d) { return xScale2(d.x) })
      .attr("cy", function(d) { return yScale2(d.y) })
      .attr("r", 5)
      .attr("fill", "rgb(116, 140, 105)");
    if ((n1star > 0.0) & (n2star > 0.0)) {
        var stable = k1/a2 > k2;
        var fillcol = stable ? "black" : "white";
        fig.append("circle")
          .datum({"x": n1star, "y": n2star})
          .attr("class", "dot")
          .attr("cx", function(d) { return xScale2(d.x) })
          .attr("cy", function(d) { return yScale2(d.y) })
          .attr("r", 6)
          .attr("fill", fillcol)
          .attr("stroke", "black")
          .attr("stroke-width", "2px");
    }
}

// 1. Add the SVG to the page and employ #2
var svg2a = d3.select("#isocline2a").append("svg")
    .attr("width", width2 + margin.left + margin.right)
    .attr("height", height2 + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var svg2b = d3.select("#isocline2b").append("svg")
    .attr("width", width2 + margin.left + margin.right)
    .attr("height", height2 + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

updateData2(svg2a, 0.8, 0.9, 0.75, 0.55)
updateData2(svg2b, 0.75, 0.85, 2.10, 1.40)

figs = [svg2a, svg2b]
for (var i = 0; i < figs.length; i++) {
    figs[i].append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height2 + ")")
        .call(d3.axisBottom(xScale2)); // Create an axis component with d3.axisBottom
    figs[i].append("g")
        .attr("class", "y axis")
        .call(d3.axisLeft(yScale2));
    figs[i].append("text")
        .attr("text-anchor", "right")  // this makes it easy to centre the text as the transform is applied to the anchor
        .attr("transform", "translate("+ (-0.7*pad) +","+(1.7*pad)+")rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
        .text("Population 2");
    figs[i].append("text")
        .attr("text-anchor", "right")  // this makes it easy to centre the text as the transform is applied to the anchor
        .attr("transform", "translate("+ (width2-1.7*pad) +","+(height2+0.7*pad)+")")  // text is drawn off the screen top left, move down and out and rotate
        .text("Population 1");
}

function linear(slope, intercept) {
return (function(xi) {
  var p = slope * xi + intercept;
  if ((p <= 2) & (p >= 0)) {
    return p;
  } else {
    return NaN;
  }
});
}
