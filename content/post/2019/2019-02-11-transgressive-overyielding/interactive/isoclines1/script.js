var pad = 50;
var container = document.getElementById("isocline1");

// 2. Use the margin convention practice
var margin = {top: pad, right: pad, bottom: pad, left: pad}
  , width = container.offsetWidth - margin.left - margin.right // Use the window's width
  , height = container.offsetHeight - margin.top - margin.bottom; // Use the window's height

// The number of datapoints
var n = 21;

// 5. X scale will use the index of our data
var xScale = d3.scaleLinear()
    .domain([0, 2]) // input
    .range([0, width]); // output

// 6. Y scale will use the randomly generate number
var yScale = d3.scaleLinear()
    .domain([0, 2]) // input
    .range([height, 0]); // output

// 7. d3's line generator
var line = d3.line()
    .x(function(d) { return xScale(d.x); }) // set the x values for the line generator
    .y(function(d) { return yScale(d.y); }) // set the y values for the line generator

// 8. An array of objects of length N. Each object has key -> value pair, the key being "y" and the value is a random number
var dataset = d3.range(n).map(function(d) { return {"x": d3.randomUniform(2)(), "y": d3.randomUniform(2)() } })
var dataset = [{"x": 0.0, "y": 0.0}, {"x": 0.0, "y": 1.0}, {"x": 2.0, "y": 0.0}];
var dataset2 = [{"x": 1.0, "y": 2.0}, {"x": 2.0, "y": 2.0}, {"x": 2.0, "y": 0.0}];

function updateData () {
  var k1 = Number(document.getElementById("iso1_k1").value);
  var k2 = Number(document.getElementById("iso1_k2").value);
  var a1 = Number(document.getElementById("iso1_a1").value);
  var a2 = Number(document.getElementById("iso1_a2").value);
  var x = k2/k1;
  var n1star = k1*(1-x*a2)/(1-a2*a1);
  var n2star = k1*(x-a1)/(1-a2*a1);
  document.getElementById("value_iso1_a1").textContent = Number(a1).toFixed(2);
  document.getElementById("value_iso1_a2").textContent = Number(a2).toFixed(2);
  document.getElementById("value_iso1_k1").textContent = Number(k1).toFixed(2);
  document.getElementById("value_iso1_k2").textContent = Number(k2).toFixed(2);
  iso1 = [{"x": 0.0, "y": k1/a2}, {"x": k1, "y": 0.0}]
  iso2 = [{"x": 0.0, "y": k2}, {"x": k2/a1, "y": 0.0}]
  svg.selectAll(".line").remove();
  svg.selectAll(".dot").remove();
  svg.append("path")
      .datum(iso1) // 10. Binds data to the line
      .attr("class", "line") // Assign a class for styling
      .attr("d", line)
      .attr("stroke", "rgb(116, 140, 105)"); // 11. Calls the line generator
  svg.append("path")
      .datum(iso2) // 10. Binds data to the line
      .attr("class", "line") // Assign a class for styling
      .attr("d", line)
      .attr("stroke", "rgb(185, 113, 79)")
      .attr("stroke-dasharray", "5px");
  svg.append("circle")
      .datum({"x": 0.0, "y": k2})
      .attr("class", "dot")
      .attr("cx", function(d) { return xScale(d.x) })
      .attr("cy", function(d) { return yScale(d.y) })
      .attr("r", 5)
      .attr("fill", "rgb(185, 113, 79)");
  svg.append("circle")
      .datum({"x": k1, "y": 0.0})
      .attr("class", "dot")
      .attr("cx", function(d) { return xScale(d.x) })
      .attr("cy", function(d) { return yScale(d.y) })
      .attr("r", 5)
      .attr("fill", "rgb(116, 140, 105)");
    if ((n1star > 0.0) & (n2star > 0.0)) {
        var stable = k1/a2 > k2;
        var fillcol = stable ? "black" : "white";
        svg.append("circle")
          .datum({"x": n1star, "y": n2star})
          .attr("class", "dot")
          .attr("cx", function(d) { return xScale(d.x) })
          .attr("cy", function(d) { return yScale(d.y) })
          .attr("r", 6)
          .attr("fill", fillcol)
          .attr("stroke", "black")
          .attr("stroke-width", "2px");
    }
}

// 1. Add the SVG to the page and employ #2
var svg = d3.select("#isocline1").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

// 3. Call the x axis in a group tag
svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(xScale)); // Create an axis component with d3.axisBottom

// 4. Call the y axis in a group tag
svg.append("g")
    .attr("class", "y axis")
    .call(d3.axisLeft(yScale)); // Create an axis component with d3.axisLeft



sliders = ["iso1_k1", "iso1_k2", "iso1_a1", "iso1_a2"]
for (var i = 0; i < sliders.length; i++) {
  document.getElementById(sliders[i]).oninput = function () {
    d3.selectAll(".series").remove();
    updateData();
  }
}

svg.append("text")
    .attr("text-anchor", "right")  // this makes it easy to centre the text as the transform is applied to the anchor
    .attr("transform", "translate("+ (-0.7*pad) +","+(1.7*pad)+")rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
    .text("Population 2");

svg.append("text")
    .attr("text-anchor", "right")  // this makes it easy to centre the text as the transform is applied to the anchor
    .attr("transform", "translate("+ (width-1.7*pad) +","+(height+0.7*pad)+")")  // text is drawn off the screen top left, move down and out and rotate
    .text("Population 1");

function linear(slope, intercept) {
return (function(xi) {
  var p = slope * xi + intercept;
  if ((p <= 2) & (p >= 0)) {
    return p;
  } else {
    return NaN;
  }
});
}

updateData();
