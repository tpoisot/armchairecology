---
title: "The great european bunny extinction of the 1950s"
subtitle: Harepocalypse now
slug: great-bunnycalypse-1950
date: 2019-02-18
topics: [population dynamics, epidemiology, virology, evolutionary biology]
uses: [d3, cite]
bibFile: "bib.json"
author: tpoisot
header:
  credit: "David Gregory & Debiie Marshall"
  license: cc-by
  source: https://wellcomecollection.org/works/jf8ggj5u
---

Rabbits have the rare combination of two important qualities: cute, and
delicious once cooked with mustard. So it is very comforting that they did not
go extinct at some time in the 1950s, which was not for lack of trying.

In fact, let's look at the data from the [National Gamebag
Census](https://www.gwct.org.uk/research/long-term-monitoring/national-gamebag-census/)
in the UK, which provides *relative* quasi-abundances for species of hunted
birds and mammals going back over a cenury -- the rabbit (*Oryctolagus
cuniculus*) is in green, and the Mountain (*Lepus timidus*) and Brown (*Lepus
europaeus*) hares are in black:

{{< includehtml "images/timeseries/layout.html" >}}
{{< includescript "images/timeseries/script.js" >}}

The rabbit population is more or less split in three epochs. From 1900 to 1940,
they seem to be frolicking merrily in the English countryside, with a relative
abundance close to unity. From 1940 to 1950, Europe was otherwise pre-occupied
with WW2 and its aftermath, and the data are not necessarily reliable. But
starting from 1950, the population experiences a sudden crash, to the point
where it reaches *almost* 0: the rabbit came very close to extinction.

It is quite notable that the hare populations seem to be largely unaffected. So
what is this thing, that could almost wipe out a species, but have little to no
effect on ecologically similar ones?

The *myxoma* virus.

At some point in the 1950s, Europe had enough of rabbits eating, well,
everything leafy. This included some crops for agriculture, but also for
decoration, and for the habitat of other species. Because rabbits reproduce
*really fast*, some people went looking for a way to control their population.
The plan, was to replicate what happenned in Australia in the early 1940s,
innoculation with the *myxoma* virus, since it there managed to control the
population and bring it back to more reasonable levels.

And so it was that in 1952, retired microbiologist Paul-Félix Armand Delille
(who was exactly as French as his name would imply) innoculated about two
rabbits from his property. Within two years, only 10% of the wild rabbit
population remained in France. The skins of dead diseased rabbits (or sick but
still alive rabbits) were shipped to a few other countries, notably the UK and
Ireland. **By 1955, rabbits were almost extinct in Europe**.

So, what saved them? Comparing the genomes of current and past strains of the
virus {{< cite "Kerr2012Evolutionary" >}}, it appears that *the virus itself*
attenuated extremely rapidly (which is the expected evolutionary outcome, but
was probably helped by the very large and fast growing rabbit population). The
original strain used by Delille came from Switzerland, was highly virulent, and
so had a very large mortality effect at the onst of infection. Seeing the trends
in population size on the figure from the National Gamebag Census, it is pretty
clear that we came close to a man-made extinction of a natural population
through biological control.

An interesting consequence is that by innoculating two rabbits, Delille was
responsible for changing the majority of the western europe landscape. There
were still idespread ecosystemic consequences only 30 years after the first
infection {{< cite "Sumption1985Ecological">}}: more agriculture, change in
relative abundances of many species due to predation shift, and decrease in the
population size of predators with a strong preference for rabbits. Less grazing
meant that vegetation succession was changed, and it is unlikely that the
ecosystem will ever revert back to where it was. **Two infected rabbits shaped
the European landscape as most of us know it**.

Don't mess with viruses, y'all.
