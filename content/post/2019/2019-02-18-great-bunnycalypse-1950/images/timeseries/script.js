var pad = 50;
var container = document.getElementById("timeseries");

var margin = {top: pad, right: pad, bottom: pad, left: pad}
  , width = container.offsetWidth - margin.left - margin.right // Use the window's width
  , height = container.offsetHeight - margin.top - margin.bottom; // Use the window's height

var xScale = d3.scaleLinear()
    .domain([1900, 2010]) // input
    .range([0, width]); // output

var yScale = d3.scaleLinear()
    .domain([0, 2]) // input
    .range([height, 0]); // output

var svg = d3.select("#timeseries").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(
        d3.axisBottom(xScale)
        .ticks(10)
        .tickFormat(d3.format(".4d"))
    );

svg.append("g")
    .attr("class", "y axis")
    .call(d3.axisLeft(yScale));

svg.append("text")
    .attr("text-anchor", "right")
    .attr("transform", "translate("+ (-0.7*pad) +","+(2.7*pad)+")rotate(-90)")
    .text("Relative abundance");

svg.append("text")
    .attr("text-anchor", "right")
    .attr("transform", "translate("+ (width-0.6*pad) +","+(height+0.7*pad)+")")
    .text("Year");

var line = d3.line()
    .x(function(d) { return xScale(d.year); }) // set the x values for the line generator
    .y(function(d) { return yScale(d.n); })
    .curve(d3.curveMonotoneX) // set the y values for the line generator

d3.dsv("\t", "data/mountainhare.tsv").then(function(data) {
    svg.append("path")
        .datum(data)
        .attr("class", "line")
        .attr("fill", "none")
        .attr("stroke", "#444")
        .attr("stroke-width", "1px")
        .attr("d", line);
});

d3.dsv("\t", "data/brownhare.tsv").then(function(data) {
    svg.append("path")
        .datum(data)
        .attr("class", "line")
        .attr("fill", "none")
        .attr("stroke", "#444")
        .attr("stroke-width", "1px")
        .attr("d", line);
});

d3.dsv("\t", "data/rabbit.tsv").then(function(data) {
    svg.append("path")
        .datum(data)
        .attr("class", "line")
        .attr("fill", "none")
        .attr("stroke", "rgb(116, 140, 105)")
        .attr("stroke-width", "3px")
        .attr("d", line);
});
