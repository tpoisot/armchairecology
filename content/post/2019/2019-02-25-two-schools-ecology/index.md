---
title: "Are there two schools of ecology?"
date: 2019-02-25
slug: two-schools-ecology
topics: [Computational ecology]
categories: [Unquantified opinions]
author: tpoisot
header:
  source: https://www.pexels.com/photo/green-leaf-photography-691043/
  credit: Arunodhai Vinod
  license: cc-0
---

> As in all sciences, ecology has its theoretical and its empirical school.
> Perhaps because of the complexity and variety of ecological systems, however,
> both schools seem, at tines, to have taken particularly extreme positions. And
> so the empiricists have viewed the theoretical school as designing misleading
> constructs and generalities with no relation to reality. The theoreticians, in
> their turn, have viewed the empirical school as generating mindless or
> mind-numbing analysis of specifics and minute.

When [Christopher Moore](http://mutualismecology.com/) shared this quote from
@LudwJone78 on twitter a few weeks ago, there was an interesting discussion
about whether this still correctly captures the spirit of ecology. While it may
have been the case in 1978 (and I can't tell, for I was not born yet, much less
practicing ecology), I think the distinction between theory and practice is
wrong now.

Not that many people may believe it exists; but it is wrong.

I think it is important to note that **ecology is not as theoretical as we think
it is** (as long as we don't count statistics). A striking result when we worked
on a survey of training needs [@BarrEzar14] is that even ecologists whose job
primarily entails modeling, are not satisfied with their mathematical skills.
These people, in turn, are a small minority of the field. There is not a
"theoretical school" as much as there are a minority of mathematically-enclined
ecologists performing their research through <q>misleading constructs and
generalities with no relation to reality</q>.

In fact, @FawcHigg12 made the interesting observation that every equations in a
biology article decreases the expected number of citations by 30%. This is a
compelling argument for (i) the fact that maths-heavy papers generate less
interest and (ii) the community which can cite these papers is smaller.

One thing that changed (well, one out of many) in the years since 1978 is the
increase in the volume of data available, and the diversity of techniques
available to analyse these data exploded -- although as @ToucMcCo16 note, we
have been, as a collective, very bad at amending our training to reflect this.
And so the theory-as-in-model *vs.* empiricism distinction is not sufficient to
capture this dimension, since different praxis of research can emerge as a
function of how much they rely on data.

In @PoisLaBr19, we suggest that the two "schools" of ecologists are instead
points on a more vast and continuous space of practices, and that somewhere in
the middle of them is *computational ecology*:

> Computational ecology is the application of computational thinking to
> ecological problems. This defines three core characteristics of computational
> ecology. First, it **recognizes ecological systems as complex and adaptive**;
> this places a great emphasis on mathematical tools that can handle, or even
> require, a certain degree of stochasticity to accommodate or emulate what is
> found in nature. Second, it understands that **data are the final arbiter of
> any simulation or model**; this favours the use of data-driven approaches and
> analyses. On this point, computational approaches differ greatly from the
> production of theoretical models able to stands on their own (*i.e.* with no
> data input). Finally, it accepts that **some ecological systems are too
> complex to be formulated in mathematical or programmatic terms**; the use of
> conceptual, or "toy" models, as long as they can be confronted to empirical
> data, is preferable to "abusing" mathematics by describing the wrong mechanism
> well. To summarize, we define computational ecology as the sub-field tasked
> with integrating real-world data with mathematical, conceptual, and numerical
> models (if possible by deeply coupling them), in order to assist with the most
> needed goal of improving the predictive accuracy of ecological research.

Seeing ecology as a theory/empiricism field is setting up an obstacle to the
unification of both sub-fields; what I believe is needed is a way to encourage
dialogue across these practices (and you can read the rest of our preprint if
you want to have more details than necessary about ways to achieve this).
Abandoning the classification of scientists as theoreticians/actual ecologists
(because let's be honest, that is sometimes what the distinction means...),
allows to move away from this binary situation into a broader variety of
practices.

This continuum of practices and profiles and skills *is* emerging as I write
this -- some training programs are taking steps to encourage it, and the volume
of data *requires* empiricists to collaborate with colleagues able to handle
<q>misleading constructs and generalities with no relation to reality</q>, also
known as abstractions. Counter-intuitively, maybe, increased ability to collect
data in a large volume means that we are becoming far more efficient at
<q>generating mindless or mind-numbing analysis of specifics and minute</q>, and
the ability to frame them within the context of workable abstractions is more
necessary than ever. The two approaches should not be pitted against one
another, but combined.
