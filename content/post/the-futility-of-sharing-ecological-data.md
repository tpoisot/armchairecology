---
title: The futility of sharing ecological data
date: 2017-07-03T09:45:33+00:00
author: tpoisot
categories: [Unquantified opinions]
topics:
  - ecological synthesis
  - open data
  - replicability
  - data sharing
---
Last week, I was part of a very interesting discussion about how data sharing in ecology has, so far, failed. <a href="http://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.1002295" target="_blank" rel="noopener">Up to 64% of archived datasets are made public in a way that prevents re-use</a>, but this is not even the biggest problem. We are currently sharing ecological data in a way that is mostly useless.

<!--more-->

**Why do we share data**?

The usual elements of response are, because their collection is publicly funded, because it makes science auditable and transparent, because open data allow _anyone_ to work on them regardless of their belonging to a close circle of colleagues, and finally because these data can be re-purposed for other studies.

![UNTITLED IMAGE](/images/wp-content/uploads/2017/07/img_0241.jpg)Most of these goals require the archival of raw data, code, and of some computational artifacts, that were used to produce the paper. The end goal is to _replicate_ or _reproduce_ the original study, either to validate it or calibrate novel methods and models, and this is best done if the original workflow is integrally available.

The last goal (re-purposing for other studies), however, requires the deposition in databases that are _open_, _persistent_, and _programmatically searchable_. We want to aggregate data _across_ studies (<a href="http://onlinelibrary.wiley.com/doi/10.1111/ecog.01941/abstract" target="_blank" rel="noopener">as we illustrated in a paper on food web reconstruction at the global scale</a>), and this is just not possible if this requires to look for thousands of studies, have to understand how the data are structured, and write the code to extract them.

**These two approaches have different focuses**.

The current view is &#8220;study-centric&#8221;, in that it packages a single study products neatly, for anyone to replicate. It is aimed at replicability, but has a limited potential to generate new insights. Oh, and as the statistic of 64% of datasets being unusable shows, we are bad at this anyways (sometimes by lack of training, and sometimes by cheating the system to get the paper published without really following the data publication guidelines).

Moving to a view that would be &#8220;data-centric&#8221;, in that data types are assigned to specified, standard databases, would be orders of magnitude better. First, it does not prevent to adopt a &#8220;study-centric&#8221; view, since the various components of the dataset can be located (and you also know what to expect, since the databases would have a standard formatting). Second, it allows fast large scale synthesis, because it is possible to write a few lines of code to hit these databases and get the  results. How fast? Look at the <a href="https://ropensci.org/tutorials/rgbif_tutorial.html" target="_blank" rel="noopener">rGBIF package tutorial</a>, for example.

**So, how do we make it happen**?

Slowly.

First, **journals** need to be more prescriptive in the ways data are archived. Putting the raw data on _Dryad_ or _figshare_ is good, but there should be additional requirements. For example, depositing all occurrence data on GBIF would be easy, and have an immediate benefit (more data for species distribution).

Second, we need to think about **standardizing** as much types of ecological data as possible. The work on species occurrences is largely done. We did a lot of work on <a href="http://onlinelibrary.wiley.com/doi/10.1111/ecog.00976/abstract" target="_blank" rel="noopener">species interactions</a> (and will expand the data format in the coming months). Functional trait data are the wild west (and the best projects have data access policy that are so bad I won&#8217;t even link them). There is a lot of rote ecoinformatics work to be done, for sure. But this is also deeply interesting work: it requires us to define how we think about data.

My opinion is  that  we are at a crossroad for the sharing of ecological data. Either we keep on doing business as usual, in which case sharing is unlikely to result in many noteworthy discoveries, and the synthesis effort will continue to give underwhelming results. Or we collectively step up, realize that ecological data are precious and relevant, and start implementing the strategies to give them their entire potential.
