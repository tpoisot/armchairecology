---
title: Responding to reviewers, part 1
date: 2017-06-19T08:30:02+00:00
author: tpoisot
topics:
  - academic writing
  - editorial process
  - peer review
---
One of the thing that made publishing easier for me was to learn how to reply to reviewer comments adequately. This can easily be overlooked, and yet understanding how it works and how to react makes a significant difference. A good response can make you skip a round of review, or can convince the editor to not reject the paper in the reviewers have strong criticisms. Everything that follows is what worked for me, and it might not work for you, and you may want to approach the problem differently. Feel free to discuss in the comments.

<!--more-->

Congratulations, your paper got &#8220;not rejected&#8221;, and so you have the opportunity to submit a revised version, along with response to the reviewers comments. Let&#8217;s walk through the process.

The first thing is to **identify who&#8217;s role is what**.

As the author, your goal is to get the paper published, and so you only represent yourself. The editor in chief (EIC) represents the journal, and their goal is to make sure that they accept papers that will have an impact. The reviewers represent your scientific community, and their goal is to ensure that the paper meets, or elevates, the standards of the field. Although reviewers can work to _improve_ the paper, both them and the EIC have a _gatekeeping_ role. The subject editor, or associate editor (AE) is on _your_ side. She decided to send the paper for review because she saw something of value in it, and most often wants to see the paper succeed (_i.e._ be accepted).

![UNTITLED IMAGE](/images/wp-content/uploads/2017/06/replies.jpg)The reviewers write their comments  for the AE, who recommends a decision, that the EIC can decide to follow or not. But your main (and often only) interlocutor in the process is the AE. And so it is _to the AE_ that you should reply with regard to reviewer comments.

This might seem counter-intuitive, but there are a few elements to keep in mind. First, there is no guarantee that the AE will solicit the same reviewers, or any reviewers at all. Second, the first thing that is done when the revised manuscript reaches the AE inbox, is to see what the replies are. If you do  the job really well, this can be the moment where the AE decides &#8220;well, this is a well done revision, and I will not send  this to the  reviewers again&#8221;. So rule number one, **explicitly address the AE in the response**.

The next thing is to **help the AE identify what you have done**.

In most cases, the AE will write a short paragraph with guidance on what to address. In some situations, if the AE is close to the field, she may write a longer review, and this is very helpful. The comment by the AE is the thing that you should read first, read at the end of the process, and read often when writing the response, to look for clues.

What clues? Well, things like, _in particular_, _especially_, _very good suggestion_, _agree with reviewer #2_, &#8230;. Basically anything that draws your attention to a criticism that the AE deems important. Do not forget that the AE has read  the reviews (and knows who the reviewers are!), and the confidential comments, and the paper, and may have an opinion on all of these things. Look for clues about which reviews are the most important, which points can be discarded. Some AE tend to be very direct (I am, mostly because I like explicit AE comments as an author), while some are more diplomatic.

The first paragraph of your response will therefore be a direct response to  this comment. Be sure to start by expressing your gratitude for the comments and the opportunity to resubmit &#8211; do not go overboard, but remember that no one is getting paid (well, except the publisher) to help you make your paper succeed, and no one is entitled to another round of revision either. When this is done, summarize the main issues, and **explicitly state how you solved them**. It may be that you disagree with what the editor or the reviewers consider to be flaws &#8211; it might be tempting to justify why you will not address the comments, but this is a bad decision. Instead, explain why you disagree, state that you understand where the disagreement comes from, and highlight what you have done to make the misunderstanding go away. Not addressing a comment in any way is very rarely a good option.

So, **where are we**?

The basic guidelines so far are, talk to the AE and not to the reviewers, express your gratitude for the comments, and state how you solved the major issues identified during peer review. This is paragraph one of your replies, and my guess is that it can make or break your paper. Spend time on it. It doesn&#8217;t need to be long, but it has to be good.

As an example, here is the first paragraph of the responses to reviewers for a recently accepted paper:

> <span style="color: #ff6600;">We would like to thank the associate editor for allowing a revision, despite numerous (and valid) points raised against this manuscript by Ref. 1. <span style="color: #0000ff;">This manuscript is indeed intended as a conceptual contribution to the field.</span></span> <span style="color: #800080;">We realize that it doesn&#8217;t justify the lack of biology we put into the first submission, and we have greatly expanded this aspect in the revision, especially at [list of pages, lines].</span> <span style="color: #008000;">We are now confident that the manuscript will appeal to all readers of [the journal].</span>

The (color-coded) structure is very simply, <span style="color: #ff6600;">acknowledgment of gratitude</span>, <span style="color: #0000ff;">defense of the overall manuscript vision</span>, <span style="color: #800080;">acknowledgment of shortcomings and how they were addressed</span>, and <span style="color: #008000;">short concluding sentence</span>. Try to be grateful, humble, and concise.

In part 2, I will discuss the response to reviewers comments proper, and how to actually write it. Stay tuned.
