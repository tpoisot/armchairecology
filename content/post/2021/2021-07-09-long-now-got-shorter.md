﻿---
title: "The “long now” of ecology just got a whole lot shorter"
date: 2021-07-09
author: tpoisot
topics: [forecasting, climate change]
---

Almost twenty years ago, Stephen Carpenter talked about the [“long
now”][longnow] of ecosystems: ecological presents and futures are shaped by historical
legacies, and events that unfolded over several human lifespans will echo
into the future of ecological systems. Last week, the unprecedented heat wave
in the Pacific Northwest killed hundreds of people; it triggered the death
of over a billion sea animals cooked alive in their shells, the shoreline
temperatures having skyrocketed past their lethal temperature. There is no
evolutionary process fast enough to rescue a population suddenly exposed to
temperatures [10°C above its T*max*][tmax] in a matter of hours. There is no model
of range displacement and landscape connectivity that we can use to devise
evidence-based policies to mitigate any of this. There is only the shift of
ecology as the chronicling of life to the chronicling of death and extinction.

[longnow]: https://esajournals.onlinelibrary.wiley.com/doi/10.1890/0012-9658%282002%29083%5B2069%3AEFBAEO%5D2.0.CO%3B2
[tmax]: https://www.jstor.org/stable/10.1086/663677

The work of [attribution on the recent heat wave is done][attribution]: it’s our
fault. We have confiscated Turtle Island from its historical stewards, and
destroyed it. The mandate of predicting the future of ecological systems
is now a Stoic intellectual exercise, *futurorum malorum præmeditatio*;
we need to predict the bad futures, we need to come to terms with the
dire consequences of our past actions, and we need to brace for what is
coming.

[attribution]: https://www.worldweatherattribution.org/western-north-american-extreme-heat-virtually-impossible-without-human-caused-climate-change/

Only we don’t have 20 years to do it. Or 20 months. Who’s to
say the next “unprecedented” event is not 20 days ahead? And precisely
because we are staring down a crisis (a trail-mix of interacting crises,
in fact), we cannot let ecology become a [crisis discipline][crisis]; we cannot fall
back on traditional ways of solving the problems, and we cannot afford to
neglect novel perspectives. When [the lives of the most vulnerable among us
are in direct and immediate danger][climateracism], understanding what lies ahead, is not
as an academic exercise; it is a moral imperative, and a matter of survival.

[crisis]: https://www.nature.com/articles/35099624
[climateracism]: https://www.nature.com/articles/s41467-021-22799-5
