---
title: Responding to reviewers, part 2
date: 2017-06-26T09:30:00+00:00
author: tpoisot
topics:
  - academic writing
  - editorial process
  - peer review
---

In the [last part](http://armchairecology.blog/2017/06/19/responding-to-reviewers-part-1/), I discussed ways to respond to the associate editor, and now it is time to discuss how to actually write the replies to the reviewers. This is a frustrating exercise, but one that can be made constructive if you try to find, in each response, a way to make your article better. Let&#8217;s dig in!

<!--more-->

But first, **a few things  to read**.

The British Ecological Society has put together a <a href="http://www.britishecologicalsociety.org/publications/guides-to/" target="_blank" rel="noopener">number of guides</a>, that are free to download. I use them in my classes very often, because they manage to distill, in a few pages, what the community of ecologists considers to be &#8220;best practices&#8221;. The guides on _Getting published_ and _Peer review_ are full of good advice, and reading them is time well spent. And now, on to the methodology.

The first thing to do, is **wait**.

We love the papers we submit, and so reading criticism of them can be harsh. So read the reviews, and then wait. I usually wait about a week, which gives me time to vent a bit, cool off if need be, but also allows me to come back to the reviews with a more impartial eye. In some situations, if I find a reviewer comment particularly outlandish, <a href="https://en.wikipedia.org/wiki/Catharsis" target="_blank" rel="noopener">I write the reply I <em>would like</em> to write</a>; then I delete it. It helps.

Being impartial is very important, because the next thing to do is to is to **triage**.

Not all reviewer comments are created equal. Some are correct, some are wrong. Some require major work, some are just about syntax. Some are suggestions, some are requirements. Some are written in a respectful way, others are insulting. But you need to reply to all of them. I usually read through all the comments a few times, then take notes (a single sentence or a few words) about what to change and how to reply.

Instead of going through every comment, this lets identify the points that were shared by all reviewers, and solve several of them at once. It also helps me identifying comments that require a lot of work, as well as comments that I do not think I will address (but more on that later). Once the triage is done (and once the changes are made!), it&#8217;s time to reply.

Reply to (almost) **everything**.

![UNTITLED IMAGE](/images/wp-content/uploads/2017/06/polarbear.jpg)I love this photo. It was taken at <a href="http://www.mcgill.ca/redpath/" target="_blank" rel="noopener">McGill&#8217;s RedPath museum</a> a few months ago, and features what is obviously not a polar bear, with the label polar bear on it. The actual polar bear is just out of frame. Our manuscripts are full of things like this &#8211; sentences that, when you look at the &#8220;big picture&#8221;, are correct, but do not hold up to close examination. The reviewers will point them out, and it is important to correct them.

Replying to everything is important, because it lets the AE know that you have done the work. At its core, a reply is always &#8220;We have [describe the change] &#8211; see [page and line number]&#8221;. Most of the minor comments will be addressed this way. It is boring. It is tedious. It is absolutely necessary.

**But what if you do not want to make a change**?

This is fine. The reviewers, like you, can make mistakes  &#8211; and  therefore their comments are not a to-do list, but a list of suggestions, which you are allowed to discuss. You can decide not to make some changes. But it needs a strong justification, because there is only a very limited number of things you are reasonably allowed to ignore before the editors and reviewers start doubting your good faith.

When I decide to disregard a comment, I usually writes a full paragraph explaining why, and most importantly why addressing the comment would not improve the manuscript. And then, I try to make some changes to the problematic parts of the manuscript  that still show that I am willing to make efforts.

**To summarize**&#8230;

Put on your poker face, pretend that every single comment the reviewer made are the most insightful thing you have ever read (some of them will be), and do your absolute best to reply to every single comment. It&#8217;s better than starting the whole process again at another journal!

&nbsp;
