---
title: "Hypotheses are over-rated. Don't let them ruin your writing."
date: 2017-09-18T08:30:57+00:00
author: tpoisot
layout: post
comments: true
guid: http://armchairecology.blog/?p=1398
redirect_from: /2017/09/18/hypotheses-are-over-rated-rewrite-them/
topics:
  - academic writing
  - scientific communication
  - scientific writing
  - statistics
  - hypothesis testing
---

It starts, like all great stories do, with Lavender. When I was a PhD student, one of my projects was to study time-series of bacteria-bacteriophage infection networks in the soil. We had a little plot of soil, about 10 cm by 50 cm, in one of the remote university &#8220;green&#8221; spaces (Montpellier in the summer is the brownish kind of green). On day 1, we got our five soil samples, and started isolating them in the lab. Then after a week, we thought it was time for another sample. Took a fork and some falcon tubes (we were good at improv science), headed down the stairs. No more soil plot. It had been replaced by the loveliest arrangement of rocks, fresh soil, and lavender plants I had ever seen. No more plot, no more sampling, no more project, no more paper.

<!--more-->

Or that would have been the situation had we blindly stuck to some interpretation of what science is and how it should be done. All starts with an hypothesis. Everything you do is aimed at testing this hypothesis, and the greatest intellectual fault is to modify the hypothesis in any way. Your initial insight is perfect, sacred, and nature would be wise to conform to what you had planned.

**Not a big fan of this approach**, as you may have guessed. Hypotheses may have some value when you start a project. They give a general idea of the direction you should take. They can help you determine the scope of your project. But then again, all of this is true of a good _What if?_ question. Among the papers I wrote, my three favorite started _not_ with an hypothesis, but with discussion where the sentence _Hey wouldn&#8217;t it be cool if&#8230;_ featured prominently.

But instead of discussing why I don&#8217;t see [hypotheses as central to a strong research program](http://bit.ly/MetSciResProg), I would like to discuss how **sticking to hypotheses can lead to less interesting papers**. When writing, keep in mind the words of David Douglass:

> It is absolutely essential that one should be neutral and not fall in love with the hypothesis.

First, research is an iterative proccess. We start asking a question because we are in a state of ignorance, and hopefully every new result brings us some new knowledge (and there is, in this mindset, no _negative_ or _positive_ results anymore). It makes sense to see how our initial idea can be bettered by including this new knowledge. This also lets you write papers with a strong narrative flow; any result follows logically from the next, and move towards the conclusion.

When approaching the structure of papers this way, it becomes very important that every figure, every table, every paragraph, has a purpose and an order. Starting from a _problem_ (or a _question_), rather than from an hypothesis, also gives an incentive to present results that move the story further. Not that this is impossible to do with a hypothesis-test-conclusion structure, but it is more difficult.

Second, I don&#8217;t like frequentist statistics to tell me how to think. This is probably far fetched, but maybe we are so obedient to The Hypothesis because frequentist statistics have diminished science to a search for significant results. A common argument against discarding and reformulating hypotheses is that it leads to _p_-hacking. I think the opposite is true; considering that the hypothesis is unmovable leads to taking whatever statistical shortcuts are required to reject it (or the null). I much prefer &#8220;Bayesian writing&#8221;, where I update my posterior as soon as I uncover new evidence.

In a sense, my initial hypothesis becomes far less relevant the further along I am in the process. If I develop better insight by accumulating new results, it is disingenuous to stick with the initial, uninformed hypothesis in the paper. Chances are I would have discarded this hypothesis long ago, or refined it to a point where it is no longer the same.

**There are, of course, risks attached to not caring very much for your hypotheses.** It is easy to stray to far from the original point. It is easy to behave entirely like a Markov-Chain, and lose track of both the starting point and the destination. My own strategy against this is to set boundaries on the project, usually in the form of concepts or questions that are &#8220;off-limit&#8221; in the current manuscript (but often make for good follow ups). It is also tempting to discard some results; there is no easy way to prevent this, besides one&#8217;s own work ethics.

We ask questions because we have some ignorance left in us, and so it is unlikely to predict all of the possible outcomes. Sticking to hypothesis means that our entire writing is framed by half-formed ideas, and prevents us from really building a compelling narrative. Hypotheses are useful to keep in mind, and it is important to be able to formulate them (especially as part of one&#8217;s training and eduction). But I&#8217;m not here for the hypotheses. I&#8217;m here for the results, and the new knowledge, and the new uncertainty they bring. I care about presenting the facts in a compelling way, more than I care about starting a paper with my initial reckons. If an hypothesis stands in the way of expressing this, it will be discarded faster than you can say &#8220;hypothetico-deductive model&#8221;.

**But what I do is pie-in-the-sky, fundamental, theoretical research**. There are fields where pre-registration, and a better accountability with regard to the initial hypothesis and why it was deviated from, are far more important, because the stakes are far greater. Theoretical research is allowed to tabe some liberty with its initial hypotheses, because no one reads papers with equations in them anyways.

And what about The Great Lavender Incident of 2010? We managed to turn it around, and find something to do after karma decided to prevent us from testing our hypothesis. [I even think it turned out quite well](https://ecologicalprocesses.springeropen.com/articles/10.1186/2192-1709-2-13).
