---
title: The artist and the craftsman.
date: 2017-05-08T09:30:36+00:00
author: tpoisot
categories: [Unquantified opinions]
topics: [teaching, research]
---

Deep down, I feel like there are two types of person in me when I am confronted to a science problem. First, there&#8217;s the artist. The big-picture, pie-in-the-sky guy, with creativity, and moxie, and a &#8220;We&#8217;ll sweat the details later&#8221; kind of attitude. And then, there&#8217;s the craftsman. The stickler for details, the one in charge of making sure that everything is clear, the one who understand the tools and knows how to use them. This is maybe especially important for computational research, but we need to understand how these two persons-in-us, the artist and the craftsperson, interact.

<!--more-->

Scene: Artemisia Gentileschi is in her workshop, and in her head is the very clear picture of what __Judith Slaying Holofernes__ will be. She walks up to the canvas, and picks up a brush. She brings it to her eyes, and start looking at it, very intensely. Then she looks around, and yell &#8220;Apprentice! What even is this weird hairy thing? Figure it out and go paint me a masterpiece!&#8221;.

**The artist needs the craftsperson**.

A lot of the great artists were _also_ masters of their craft, and pushed it in novel directions. When Perec wrote _<a href="https://fr.wikipedia.org/wiki/La_Disparition_(roman)" target="_blank" rel="noopener noreferrer">The Disparition</a>_, it was not only a work of literary art. It was an exercise in pure technical mastery: the novel is written without using the letter _e,_ the single most frequent in French, and is _full_ of hints about what is going on without ever being explicit.

![UNTITLED IMAGE](/images/wp-content/uploads/2017/04/cropped-img_20161129_1504511.jpg)

But whats does it have to do with research? Having ideas, ambitious ideas, is good. But this is only the first part of the effort. Being able to follow through on these ideas is important too &#8211; more important, even! I have notebooks with pages full of good ideas that I don&#8217;t quite know how to pull off yet. I know several groups in which students cannot go seek independent statistical advice until they have read extensively on the  subject, and have developed the necessary expertise to do the work themselves. Regardless of the merit of this stance, I can understand the motivation: we need to be able to realize our vision, and this requires to master the craft.

**The craftsperson needs the artist**.

But here is the B-side: the craftsman in me is not the most creative guy in the room. And so if I were to silence him, I would probably produce very dry, very uninspired, albeit technically perfect, research. The balance, and it is difficult to strike, is to make sure that none of these halves are subservient to the other. It&#8217;s no wonder <a href="https://www.theguardian.com/music/2006/jul/16/popandrock.shopping" target="_blank" rel="noopener noreferrer"><em>Run DMC</em>'s eponymous 1984 album "changed music"</a>: they managed to establish the MC (the artist) and the DJ (the technician) as equal halves, and paved the way for NAS and Public Enemy.

At the end of the day, all the technical skills in the world are not useful if they are not put to use within the context of creative questions. I am always a bit weary when I hear people dismiss the importance of _owning_ the technical side of things, or when I hear other saying that pure technical skill is enough to make great science.

We need both. Not always in equal parts, but we need a healthy balance of having ideas, and being able to implement them. There is even something relaxing to it! When I&#8217;m not in the mood to pull through the grind of technical work, I can stop for a moment and imagine things. When I&#8217;m frustrated with blue-sky thinking, I can _make_ a thing. Having an equal part of artist to my craftsman guarantees that I am never bored.
