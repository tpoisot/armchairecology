---
title: Is community ecology a thing?
date: 2017-07-17T09:45:29+00:00
author: tpoisot
topics: [community ecology, epistemology]
uses: [cite]
bibFile: bib.json
---

I was talking with a friend about a conversation they had, where someone
questioned the fact that "community ecology" was a field/concept/thing. My own
opinion of this, as a sometimes self-described community ecologist, is obviously
"yes it is". But let's entertain the idea that it is not, and justify its
existence.

<!--more-->

**Community ecologists have always asked this question**.

Going back to <a href="http://www.jstor.org/stable/3546712?seq=1#page_scan_tab_contents" target="_blank" rel="noopener">Lawton&#8217;s 1999 paper in Oikos</a> (&#8220;Community ecology is a mess&#8221;), up to the very important ASN presidential address by Dan Simberloff {{< cite "-Simberloff2004Community" >}} -- <q>[this] begs the question of why ecologists [continue to study] community ecology</q> -- one of the field&#8217;s most exciting question has seemingly been &#8220;Wait, are we losing our time here?&#8221;. Of course both Lawton and Simberloff conclude that community ecology is well worth studying.

In parallel, key papers from recent years have been about figuring out a way out, and specifically a conceptual framework to probe questions in community ecology in a way that is integrative. I am of course talking about <a href="http://www.journals.uchicago.edu/doi/abs/10.1086/652373" target="_blank" rel="noopener">Mark Vellend&#8217;s proposal to unify community ecology to evolutionary biology concepts</a>, and <a href="http://www.sciencedirect.com/science/article/pii/S0169534706000334" target="_blank" rel="noopener">Brian McGill&#8217;s et al. appeal for more traits</a>. There are dozens of additional references I could link to here, that suggest things we need to add to make community ecology slightly easier (<a href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3917341/" target="_blank" rel="noopener">interactions</a>; more biotic interactions make everything better).

**But do we need community ecology at all**?

Imagine a world where we have invented and established the field of population ecology, but not community ecology. We can describe changes of population size with regard to the environment and their own intrinsic factors. We have a notion of what a metapopulation is. We can read about it in, for example, <a href="https://books.google.ca/books/about/Introduction_to_Population_Ecology.html?id=k5MLmvuDnUsC&source=kp_cover&redir_esc=y" target="_blank" rel="noopener">Larry Rockwood&#8217;s excellent textbook</a>. And it is going to be a considerably shorter read, since it stops at part I. Part II (_Species interactions_), because it introduces multiple populations interacting, is already working at a scale of organization which encompasses populations. <a href="http://ca.wiley.com/WileyCDA/WileyTitle/productCd-EHEP002287.html" target="_blank" rel="noopener">It&#8217;s community ecology</a>.

The transition between the two scales is qualitative. It is a discrete step, because it increases the scope of (i) mechanisms that must be accounted for, and (ii) situations that can be studied. Because community ecology is essentially population ecology, but with several populations, it also becomes a field with challenges of its own, that are not visible at the more basal scale. The same thing happens to community ecology when we scale up to ecosystem ecology.

Community ecology is a superset of population ecology: it must contains all of population ecology, as well as additional concepts (those that are relevant to linking these populations together). And this justifies its existence as its own field, because population ecology is necessary, but not sufficient to _solve_ community ecology.

The interesting point is that, just like ecosystem ecology has not deprecated community ecology, community ecology does not replace population ecology (though we can probable teach the three fields in two classes; populations to communities, and communities to ecosystems). Any improvement at a more basal scale will cascade _up_ to its supersets &#8211; being able to describe the behavior of populations better will inform community ecology.

One of the few things ecologists agree on is that, <a
href="http://onlinelibrary.wiley.com/doi/10.2307/1941447/abstract"
target="_blank" rel="noopener">when the scale of observation changes, the
mechanisms and their consequences change in surprising ways</a>. The added
twist, is of course that some consequences are due to mechanisms acting at other
scales. In itself, this justified (in my mind) the existence of sub-fields
focused on a particular scale. The frontier between population/community and
community/ecosystem ecology is often fuzzier than we care to admit, but this
area of fuzziness, to come back full circle and quote Simberloff, <q>is the
locus of exciting advances, with growing mechanistic understanding of causes,
patterns, and processes</q>.
