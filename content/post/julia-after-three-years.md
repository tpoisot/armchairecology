---
title: Three years of using Julia
author: tpoisot
date: 2018-03-12
topics:
  - programming
  - julia
---

I started using the [Julia] programming language in 2014, and by 2015, it
represented about 75% of my programming time/output. These have been interesting
years, because the language changed a lot, but also because I found a tool that
makes me very productive.

[Julia]: https://julialang.org/

<!--more-->

My two favourite pieces of cookware are a Victorinox chef knife, and a Lodge
cast-iron skillet. I use the chef knife to do things it's not really meant to do
(like garlic purée), or things that should probably be done with a paring knife.
But I'm efficient with it, and it makes me happy. The same goes for the
cast-iron. Some things should probably be done in non-stick, but I don't care. I
love doing them this way, and they turn out alright in the end.

Programming languages are the same: they are *tools*. A good tool is one that
makes you productive, happy, and allow you to deliver a usable product. For me,
`Julia` allowed all of that, and this is in large part because of the things I
do often (write custom code to do various numerical things), the things I do
somewhat often (differential equations, embarrassingly parallel problems), and
the things I don't do very often at all (statistics).

The one area where `Julia` shines is that it eliminates the need for
intermediate steps when preparing code. For one of my first large simulation
projects, I wrote initial proof of concept code in `R`. Then I moved it to
`python`, and after spending some time identifying bottlenecks, I re-wrote these
parts in `C`. I don't do this anymore: I use the same language for proof of
concept, prototyping, and production. And because I spent a bit of time reading
the [performance tips][pft] section of the manual, the code for the three steps
is often very similar. This is a huge time saver, and it decreases the cognitive
load associated with switching languages.

[pft]: https://docs.julialang.org/en/stable/manual/performance-tips/

One thing I have noticed over the last two years is that my use of supercomputer
time decreased. Because the [parallel computing][parcomp] abilities out of the
box are really strong, most of what I need can be done on my 32-cores desktop.
In a recent instance, porting code from `python` to `Julia` decreased the time
of execution by two orders of magnitudes: investing the time to make this run on
a cluster, and waiting for a spot in the queue, was not worth it anymore.

[parcomp]: https://docs.julialang.org/en/stable/manual/parallel-computing/#Parallel-Computing-1

And now is the right time to start learning the language. The [Discourse]
community is very quick at answering, and very welcoming of newcomers. The
`v0.7` will be stable (or very close to), so there will be no more deprecation
when `v1.0` is released. The ecosystem of packages is starting to diversify, and
[JuliaDB] is a very good solution for data crunching.

[Discourse]: https://discourse.julialang.org/
[JuliaDB]: http://juliadb.org/
