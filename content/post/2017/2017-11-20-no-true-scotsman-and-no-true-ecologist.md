---
title: '"No true Scotsman" (and no true ecologist)'
date: 2017-11-20
slug: no-true-scotsman-and-no-true-ecologist
author: tpoisot
categories:
  - Unquantified opinions
topics:
  - academia
  - teaching
---
<!-- wp:paragraph -->

It is not a surprise that academia is built around gatekeeping. Admissions to undegraduate programs are competitive. Admissions to graduate schools are competitive. Scholarships are competitive. Positions are too. And publications. These are features of the system, which we must live with. But something more pervasive is general statements about what makes a &#8220;true ecologist&#8221;. There is no such thing.

<!--more-->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

In the last weeks, there has been a lot of discussion about the list of the most important articles in ecology and evolution. [Terry McGlinn has a nice writeup about the issues surrounding this list](https://smallpondscience.com/2017/11/14/what-are-the-top-100-must-read-papers-in-ecology/). The message of the list was clear: to be considered a member of the ecologists community, here are the 100 things you must (i) have read and (ii) appreciate as keystones of our field.

A few days ago, Dynamic Ecology published a blog post about there being [only two kinds of ecologists](https://dynamicecology.wordpress.com/2017/11/16/the-two-kinds-of-ecologists/) (you like differential equations, or you like regression). Being an ecologist implies that you adhere to one of two (non exclusive, by the way) mathematical formalisms; you can&#8217;t use different tools to comprehend the world depending on which performs best (or just ditch the mathematical formalisms entirely).

Last week, too, a research consortium I am a member of started a contest: share your best PhD stories, and the winner gets a coupon for outdoor gear, and a rain-proof notebook. Being a biodiversity scientist obviously means that these are things you use routinely.

More broadly, I have been involved in a number of conversations in the last years where people had opinions like &#8220;X is the only way to do ecological research&#8221;, or &#8220;you need to attend a class on Y to be a real biologist&#8221;. In all instances, this translated directly to &#8220;I wish everyone was more like me, and enjoy the things I enjoy&#8221;. **This is a deeply problematic attitude**.

As a computational ecologist, I _love_ talking with my fellow indoors scientist. It&#8217;s fun. It genuinely is. But it does not make me grow as a scientist. Talking with people with whom I have very little common ground does. Some of my most exciting emerging collaborations are with colleagues that have very different research programs. Because **complementarity in interests and experience makes for better science**.

My issue with the &#8220;you must name this many tree species to be an ecologist&#8221; argument is that, eventually, it will lead to a very homogeneous community. If everyone attended the same classes, read the same papers, and has the same approach to research&#8230; what is the point? It would be **boring**.

Not only are these statements wrong (there are as many ways as being an ecologist as there are ecologists), they are damaging. Every time someone establishes &#8220;X&#8221; as the right way to do things, those doing &#8220;not X&#8221; hear that they do not belong, are not legit, should not be here. **As ecologists, we should know that more diverse communities are more productive**. It&#8217;s time we apply this finding to the way we think about our field.
