---
id: 1237
title: Bipartite networks are not sampled well.
date: 2017-07-24
slug: bipartite-networks-are-not-sampled-well
author: tpoisot
layout: post
comments: true
guid: http://armchairecology.blog/?p=1237
redirect_from: /2017/07/24/bipartite-networks-are-not-sampled-well/
publicize_twitter_user:
  - tpoi
  - tpoi
publicize_google_plus_url:
  - https://plus.google.com/+TimothéePoisot/posts/DMkuMk84uNk
  - https://plus.google.com/+TimothéePoisot/posts/DMkuMk84uNk
categories: [Semi-quantified opinions]
topics:
  - biases
  - gradients
  - macroecology
  - ecological networks
---
There is a whole sub-genre of the ecological network literature working on elucidating &#8220;the structure&#8221; of bipartite networks (parasite/host, pollinator/plant, &#8230;). I am, of course, guilty of contributing a few papers to this genre. The premise is that, by putting together enough data from different places, we may be able to infer some of the general mechanisms that shape different aspects of the structure.

<!--more-->

After following a few conversations on twitter, notably by <a href="https://twitter.com/brunalab" target="_blank" rel="noopener">Emilio Bruna</a> on the deficit of data in the South, and by <a href="https://twitter.com/cricketcrocker" target="_blank" rel="noopener">Katherine Crocker</a> on the importance of territorial issues in science, I wanted to look at how well the current available networks sampled the latitudinal gradient (because this is the low hanging fruit for these questions). Sampling the gradient well is important, because we can only describe well what we sample well, and if our sample is biased towards certain regions, then our &#8220;general&#8221; trends implicitly assume that the unsampled regions do not matter.

How do bipartite ecological networks fare?

**Poorly**.

![UNTITLED IMAGE](/images/wp-content/uploads/2017/07/screenshot-from-2017-07-23-10-55-01.png)The map of the left is the current map of data from <a href="http://mangal.io/data/" target="_blank" rel="noopener">mangal.io</a> (_v2_ coming soon). Looking at the raw data, I was surprised to see how rare latitudes lower than 0 where. Our knowledge of these systems is heavily biased towards the northern hemisphere. In fact, if we look at  the distribution of latitudes for a subset of these data, it becomes a little bit clearer: not only do we sample more, overall, at higher latitudes, but the range of positive latitudes that is covered is larger (0 to ~ 90, _vs_ 0 to -60).

![UNTITLED IMAGE](/images/wp-content/uploads/2017/07/screenshot-from-2017-07-23-11-06-06.png)

Part of this is because the landmass distribution is not uniform, but this cannot be the only explanation. There are almost no data in Africa, and a handful in South America. This is in stark contrast with the situation in Europe or North America.

**This may be a problem**.

As I mentioned earlier, our &#8220;general&#8221; inference about networks are only really robust in the area we describe well. At this point, although we _can_ compare the structure of networks in space, any general discussion on the latitudinal  trends has to be presented with the caveat that some areas are virtually not sampled.

And we have some clues that the situation at lower latitudes is not simply a mirror of the situation at higher ones. When we <a href="http://onlinelibrary.wiley.com/doi/10.1111/ecog.01941/abstract" target="_blank" rel="noopener">reconstructed food webs from distribution and interaction data</a>, we found no latitudinal gradient in connectance (and because of the co-linearity between networks measures, this means that a latitudinal gradient in other measures is unlikely).

![UNTITLED IMAGE](/images/wp-content/uploads/2017/07/screenshot-from-2017-07-23-11-14-50.png)

Of course I firmly believe in aggregating data from different sources to infer some more general rules. But we (myself very much included) need to be careful about the implicit assumptions: &#8220;sampled&#8221; does not mean &#8220;general&#8221;, &#8220;unsampled&#8221; does not mean &#8220;unimportant&#8221;, and &#8220;existing&#8221; does not mean &#8220;sufficient to make inference&#8221;.

By  the way, if you were expecting a discussion of the sampling of _one_ ecological network, then Pedro Jordano wrote <a href="http://onlinelibrary.wiley.com/doi/10.1111/1365-2435.12763/abstract" target="_blank" rel="noopener">one of the most insightful papers on the topic last year</a>. Reading up on how difficult it is to sample a single network should put the problem of sampling all the networks everywhere in perspective&#8230;
