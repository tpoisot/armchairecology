---
title: Ecology's unhealthy obsession with patterns
date: 2017-04-17
slug: ecologys-unhealthy-obsession-with-patterns
author: tpoisot
categories: [Semi-quantified opinions]
topics: [patterns, scientific writing]
---

Don&#8217;t we love patterns? This is, after all, one of the purpose of ecology as a science: to describe and document what it is, exactly, that species and individuals and communities and ecosystem _do_. And in order to do so, we look at them, and transcribe what we see. And so if you happen to find a pattern, this is noteworthy ecology that should be published, whereas the lack of a pattern means the opposite. And now, please, this madness has to stop.

<!--more-->

**I hate patterns**.

Let me clarify. There are, absolutely, things in nature that can be described as _patterns_. I am fine with this. What I dislike somewhat intensely is the use of the _word_ pattern, and the reason is really quite simple. Almost all of the things that can be called _a pattern_ can be called something else instead. Something more informative, something more correct, in short something _better_.

Because I guess this is a point in need of proving, I downloaded PDFs from the latest issue of _Global Ecology & Biogeography_, and started reading. Why _GEB_? Because they publish great science, so reading through an entire issue was quite enjoyable. Out of the nine research papers I read, I counted 142 uses of the word _pattern_. The relevant information, of course, is to understand how this word is used.

After a bit of careful reading, I found six uses: the distribution of something (usually a species) in time and space; the fact that something has a non-random structure in time and space (as in, for example, the distribution of trait values); the existence of either invariant properties or of a series of observations; a statistical relationship; a statistical distribution; and last but not least, uses of the word where I was either unable to guess the meaning, or where it could have been removed.

![](/images/wp-content/uploads/2017/04/patterns.png)

The distribution of the different cases is on the left &#8211; every band correspond to a single paper, and the values have been scaled (because each paper had between 4 and 40 times the p-word). This way, a very high peak mean that papers consistently use pattern to mean this thing. The relative size of the bands is proportional to the frequency where each paper uses pattern for which meaning.  There are two things worth noting. First, &#8220;the existence of invariants or repeated observations&#8221; is by far the most consistent meaning of _pattern_. Second, _pattern_ is often used to describe things with a more defined meaning.

So what do we learn from this very unscientific analysis? Pattern is a catch-all term, and it means different things both within and across papers.

**And this is a problem**.

In a number of situations, _patterns_ is used in place of a more with a more specific meaning. For example, _correlation_. When I read &#8220;other authors reported the same positive correlation between dispersal and generalism&#8221;, I have all the information I need. When I read &#8220;the same pattern was reported by other authors&#8221;, I have to go back to the results to see what this _pattern_ is. When using _pattern_, we tell that there is something. What we ought to do is _show_ this thing instead.

But more importantly, because pattern is used to mean different things between and across papers, when you write it, you expect the authors to pick the right meaning when they read your text. **This is maybe the worse mistake of all in scientific writing: you lose control of how your text should be interpreted**. Over-using pattern over a word with a more specific meaning means that your text conveys less information, and has more ambiguity. This is just sloppy writing.

A colleague and good friend of mine has a less charitable theory &#8211; and I will share it because it is interesting to consider. Using pattern is not sloppy writing, but is a symptom of sloppy thinking. _Pattern_ is a shortcut to get papers out as fast as possible. &#8220;Look, there&#8217;s something in the data, and it may be a distribution or a correlation or a gradient, but it has a p-value so let&#8217;s publish it&#8221;. Instead of pausing to carefully consider what this thing is, we just call it a pattern, and then call it a day.

There is a kernel of truth in this theory, I think, as attested by the number of times I&#8217;ve heard discussions about the &#8220;quest for patterns&#8221;. The issue with this stance is that patterns are so many different things, that if we are looking for them, we are left looking for something, anything. And so I think we should retire this word for a little while, and see whether it results in clearer papers. In his _Advice for a Young Investigator_, <span class="addmd">Santiago Ramón y Cajal </span>cites <a href="https://books.google.ca/books?id=gK4xNnSDQ9YC&pg=PT80&dq=A+well+chosen+word+can+save+an+enormous+amount+of+thought&hl=fr&sa=X&ved=0ahUKEwjTpqmUoaTTAhVkwYMKHZGYCn4Q6AEIJTAA#v=onepage&q=A%20well%20chosen%20word%20can%20save%20an%20enormous%20amount%20of%20thought&f=false" target="_blank">Mach</a>: <q>A well-chosen word can save an enormous amount of thought</q>. Pattern does the opposite: it is what we use when we refuse to carefully chose a word, and it leaves our reader with more thinking to do as a result.
