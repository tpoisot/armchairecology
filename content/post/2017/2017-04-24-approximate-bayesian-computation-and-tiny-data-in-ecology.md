---
title: Approximate Bayesian Computation and tiny data in ecology
date: 2017-04-24
slug: approximate-bayesian-computation-and-tiny-data-in-ecology
author: tpoisot
categories:
  - Adventures in mathematics
topics:
  - bayesian inference
  - computational ecology
  - data science
  - statistics
---
Every time I hear about _Big Data_ in ecology, I cringe a little bit. Some of us may be lucky enough to have genuinely _big_ data, but I believe this is the exception rather than the norm. And this is a good thing, because _tiny data_ are extremely exciting &#8211; in short, they offer the challenge of isolating a little bit of signal in a lot of noise, and this is a perfect excuse to apply some really fun tools. And one of my favorite approaches for _really small data_ is ABC, Approximate Bayesian Computation. Let&#8217;s dig in!

<!--more-->

It&#8217;s approximate, Bayesian, and computational. **What&#8217;s not to like**?

In a nutshell, <a href="https://en.wikipedia.org/wiki/Approximate_Bayesian_computation" target="_blank" rel="noopener noreferrer">ABC</a> is a method to infer model parameters, when the likelihood of the model is difficult to calculate &#8211; or more honestly, when you cannot be bothered to calculate it. You should really click the link before, the Wikipedia page was adapted from <a href="http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1002803" target="_blank" rel="noopener noreferrer">a <em>PLOS Computational Biology</em> topic page which is well worth reading</a>.  One of the most famous applications of ABC has to do with counting <a href="http://www.sumsar.net/blog/2014/10/tiny-data-and-the-socks-of-karl-broman/" target="_blank" rel="noopener noreferrer">how many socks Karl Broman has</a>, because dealing with quantitative techniques all days changes your brain in surprising ways.

![UNTITLED IMAGE](/images/wp-content/uploads/2017/04/screenshot-from-2017-04-21-15-43-29.png)ABC works by comparing the output of a model to empirical data, through summary statistics. Instead of comparing the raw data/output, they are first summarized, and these summaries are compared. If the summaries are close enough, then the parameters that were used for this simulation run are a good representation of reality. It seems simple, because it actually is. The interesting feature here is that summary statistics are used, and so a wide range of models can be compared to empirical data. Specifically, as long as you can apply these summary statistics to the model output, then it is possible to use it in the context of ABC.

In ecology, <a href="http://www.annualreviews.org/doi/abs/10.1146/annurev-ecolsys-102209-144621?journalCode=ecolsys" target="_blank" rel="noopener noreferrer">this is a very important feature</a>. It lets us use models that can be <a href="http://rsif.royalsocietypublishing.org/content/6/31/187" target="_blank" rel="noopener noreferrer">quite arbitrary</a>, and because the method fits in the Bayesian framework, it estimates the distribution of the parameters of these models. In practice, this also means that because the number of data that are put in for the estimation can be small, ABC allows inference based on tiny data. For example, <a href="http://onlinelibrary.wiley.com/doi/10.1890/13-1065.1/abstract" target="_blank" rel="noopener noreferrer">ABC SMC schemes have been used to estimate parameters in age-structured population models</a>, which would be a very difficult thing to do should the likelihood have to be expressed.

One of the issue we have in ecology (and probably in other fields) is that the models do not always adequately capture all of the subtlety of empirical data. ABC is a way of coming up with a model that may be very phenomenological (remember, there is no need to produce a directly comparable dataset as long as you can use summary statistics), and give a &#8220;good enough&#8221; idea of the values and distributions of underlying parameters.

What I like with this approach, and where I think its inferential power lies, is that we can build toy models, and compare them to small amounts of data. Then, investigating the posterior distribution of the various parameters can give insight about what is important, what is not, and the relationship between the values of different parameters. At the core of <a href="https://en.wikipedia.org/wiki/Computational_thinking" target="_blank" rel="noopener noreferrer">computational thinking</a> is the loop of problem formulation (empirical data), solution expression (conceptual model), and solution evaluation (ABC). This approach fits ABC in a very natural way.

I think this method can deliver some interesting results in ecology (as it already did in other fields). The links in this post should give you an idea of where to start &#8211; this is a surprisingly simple, yet astoundingly powerful method, and I definitely think it will get more traction in ecology in the short term.
