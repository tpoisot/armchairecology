---
id: 1304
title: The most important scientific figure ever drawn.
date: 2017-07-31
slug: the-most-important-scientific-figure-ever-drawn
author: tpoisot
layout: post
comments: true
guid: http://armchairecology.blog/?p=1304
redirect_from: /2017/07/31/the-most-important-scientific-figure-ever-drawn/
publicize_twitter_user:
  - tpoi
  - tpoi
publicize_google_plus_url:
  - https://plus.google.com/+TimothéePoisot/posts/NN2V7yoH8Xm
  - https://plus.google.com/+TimothéePoisot/posts/NN2V7yoH8Xm
image: cholera-bacteria-2xujmg4fnx3n2dcxdwxv62.jpg
categories:
  - Unquantified opinions
topics:
  - epidemiology
  - history of science
  - scientific communication
  - visualization
---
A good figure is worth a thousand words. But some figures can change the world in very profound ways. Today, I would like to discuss what I think is the most important scientific figure ever drawn.

<!--more-->

**But first, context**.

The word malaria comes from 18th century Italian _mala aria_, literally _bad air_. It was thought that inhaling air around the marshes and swamp caused the disease. We know now that it is caused by _Plasmodium_, transmitted by mosquitoes. And although mosquitoes do live in swamps, the etymology of the disease name is an interesting testament to how phenomenological our approach of diseases used to be.

For a long time, the dominant theory was _miasma_ theory, which explains diseases and epidemics by the poor quality of the air. The first evidences _against_ this theory date from the 16th century, but it remained prominent among scientists and clinicians. It started to lose ground in the 1850s, when Pasteur, Koch, and a others microbiologists suggested the existence of _germs_ as causal agents for infectious diseases.

But, give or take a few decades, until the 1880s, we lacked a concept to describe, at the biological level a pathogenic agent, and the epidemiological level, an infectious disease.

In the period between 1850 and 1880, London was hit by Cholera. Then came a doctor by the name of John Snow, who decided to understand why some households were more affected than others. By 1849, Snow had already suggested that the cholera was transmitted through something which was not air. But the real breakthrough occurred in 1854, when he draw this map:

![UNTITLED IMAGE](/images/wp-content/uploads/2017/07/643px-snow-cholera-map-1.jpg){: .center-image}

Each block on this map is one patient with cholera symptoms, by address. There is a hotspot of disease in the center of Broad Street (in the middle of the map). And right in the middle of the street, is a water pump. This water pump was used by all infected households in the neighborhood. This map was enough to lead authorities to condemn the pump, and the cholera cases started to decline.

**This map (almost) changed the world**.

Many consider this map to be what founded modern epidemiology. Alongside advances in microbiology, this led to a change in how we thought about infectious diseases, with consequences for public health policy, medicine, nursing, and prevention. During the industrial revolution, most deaths in cities were due to disease. Because this map uncovered mechanisms of disease transmission, it is responsible for the increase in life expectancy in the early 20th century.

But not everywhere. There is something more important than the foundation of epidemiology to read in this map. <a href="http://www.who.int/mediacentre/factsheets/fs107/en/" target="_blank" rel="noopener">Cholera still kills hundreds of thousands of people every year</a>. Especially during the colonial age, controlling epidemic diseases resulted in a positive feedback loop, that reinforced the economic inequalities between the colonialist powers and the colonized countries. We cannot forget, when looking at this map, that advances made in the western world during the 19th century are still not applied to developing countries today. As scientists, this map speaks both of our highest achievements, and our most shameful failures; of the discovery of fundamental rules of nature, and of our using them to maintain or re-inforce the political status quo.

As far as 1854, science was political, because it was somehow decided to keep the benefits of this discovery localized to a handful of countries. In _<a href="http://ca.wiley.com/WileyCDA/WileyTitle/productCd-0745651895.html" target="_blank" rel="noopener">The Tyranny of Science</a>_, Feyerabend writes about the fact that science only improves the live of people to whom we bring it; for other, it might have never happened.  The drawing of this map changed the world: it made inequalities between developed and developing countries larger than they were. Of course it saved countless lives ever since. But we cannot celebrate this without being explicit about whose lives we choose to save, and therefore whose lives we choose not to.

The John Snow map is very well covered in _<a href="https://www.amazon.ca/Ghost-Map-Londons-Terrifying-Epidemic/dp/1594482691" target="_blank" rel="noopener">The Ghost Map</a>_, and the importance of positive feedback loops in world history is the central theme of <a href="https://www.amazon.ca/Guns-Germs-Steel-Jared-Diamond/dp/0393317552" target="_blank" rel="noopener"><em>Guns, Germs, and Steel</em></a>.
