---
id: 1408
title: Five tips to make better presentations
date: 2017-09-25T08:30:24+00:00
author: tpoisot
layout: post
comments: true
guid: http://armchairecology.blog/?p=1408
redirect_from: /2017/09/25/five-tips-to-make-better-presentations/
publicize_google_plus_url:
  - https://plus.google.com/+TimothéePoisot/posts/PfTsx7nVmNM
  - https://plus.google.com/+TimothéePoisot/posts/PfTsx7nVmNM
publicize_twitter_user:
  - tpoi
  - tpoi
image: wp-image-494686240.jpg
categories:
  - Unquantified opinions
topics:
  - scientific communication
---
It&#8217;s fall term. Do you know what this means? It means I&#8217;ll spend the next 12 weeks teaching Scientific Communication to a bunch of students, and thinking a lot about how to communicate efficiently. The first exercise we&#8217;ve done was to find an important figure, and discuss, in four slides and four minutes, how it changed the way we thought about science. It was interesting, because it helped me establish a baseline for the class (before approaching presentations in a more formal way), and also evaluate what each student will have to work on. And like the previous years, I think there are a few things that are easy to do, and would immediately improve presentations. Let&#8217;s dig in.

<!--more-->

**No default template.** We&#8217;ve seen the default templates _a lot_. Starting with the defaul template is a good way to make slides that are not memorable. This is especially true when using beamer (whose default is ugly), but applies to all presentation software. Instead, start with an empty slide (or make your own template), and create something unique. The default templates usually lack [negative space](https://en.m.wikipedia.org/wiki/Ma_(negative_space)). They feel cramped, and it is difficult to see where elements start and end.

**Care about fonts**. If you don&#8217;t want to care about fonts, [read the words of people who do](http://bit.ly/ButPraPres). This link is a good introduction, and ends with solid recommendations for slides (my own presentation template is adapted from this page). Fonts are especially important, since many of us work on 4K / retina screens, with very high resolution. We then present on something with a much lower resolution, worse color range, and probably bad contrasts. Picking fonts that work reliably in bad conditions is useful (and banning serifs from your presentations is not a good practice).

**Do not prepare** **_slides_****.** Instead, prepare _a talk_. When the talk is done, think about the visuals needed to make your points. When I ask students to do it, they usually end up with much fewer slides (which are also much better). The alternative is to make slides, then figure out what to say for each slide. This is the best way to end up with stilted talks, and awkward transitions. And also too much slides with too much text.

**Rebuild things, from scratch.** Especially figures. A figure for a paper can have much more details, because there is no limit to the time readers will spend on it. A figure for a presentation will be on screen for less than a minute, and so it should have the least possible amount of information. Increase the labels size, make bigger points, and make sure the colors match. Speaking about colors&#8230;

**It&#8217;s contrast, all the way down.** My usual guideline is to use no more than three colors in a presentation (in addition to the background and foreground colors). These three colors should have maximum contrast, and be distinguishable to _all_ people. Our short term memory cannot store a lot of information (between 4 and 7, depending on who you ask). Keeping the number of colors to a minimum, and making sure every color matches onto the same meaning every time, will help your audience.

There you have it. These five ideas boil down to _less is more_, and _build things yourself_. Visual support is here to support the audience (not the speaker!), and to do this, it should not introduce too much cognitive load. Make sure that what you offer is different enough from the default that we care, but simple enough that we can follow. There is more to making and giving presentations than these five tips, but I think they are aq good foundation for better slides.
