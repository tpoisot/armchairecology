---
title: Ecological modelling is world building
date: 2017-03-31T11:20:22+00:00
author: tpoisot
category: "Ecology & maths"
slug: modelling is world building
topics: [ecological modelling]
---

I remember the first time I have been _surprised_ by a model. I was working on
the conditions under which a mutualist can protect its host from a pathogen, and
in particular whether the mutualist can persist or will be displaced by the
pathogen (unless there are multiple populations connected by dispersal, <a
href="http://onlinelibrary.wiley.com/doi/10.1002/ece3.1151/abstract"
target="_blank">the answer is no</a>). What surprised me was how, in the end,
the answer to this question depended on the relative value of three parameters.
Of course, nothing in modeling should be surprising, because the model
encompasses the entirety of its own rules, and so _of course_ the answer is in
here, waiting to be found. But where do the models come from?

<!--more-->

**People**. Models are written by people.

And this is what I find fascinating. Modeling is the ultimate exercise in world
building. <a href="http://worldbuilding.institute/people/mark-j-p-wolf"
target="_blank">Mark J.P. Wolf</a> wrote that creating a world &#8220;_renews
our vision and gives us new perspective and insight into ontological questions
that might otherwise escape our notice within the default assumptions we make
about reality&#8221;_. But as far as ecological models are concerned, these
default assumptions are what constitutes the basis of our model. Populations
increase in size until they consume the amount of resources that the system
receives. Being eaten makes your population smaller. It&#8217;s better for your
growth rate to be adapted than to not be. Things move around. And yet, despite
these being self evident, I am often surprised by what happens when I mix them
together.

Isn&#8217;t this amazing? That when we pool together the things we know, we
create yet more things we didn&#8217;t knew yet? Which brings me to the point;
**modeling, deep down, is experimental work**.

What happens if I modify this variable? What happens if I decide to model this
process one way or another? By far my favorite example is the way to write the
logistic growth of a population. The one most often taught is

$$\frac{1}{N}\frac{\text{d}}{\text{dt}}N = r \left(1 - \frac{N}{K}\right)$$

where $N$ is the population size, $r$ is the rate at which it can grow, and $K$
is the maximal number of individuals before all the incoming resources are
exhausted. But we can write this model in another way, specifically

$$\frac{1}{N}\frac{\text{d}}{\text{dt}}N = r - \alpha N$$

where $\alpha$ is the rate at which the individuals will compete for the
resources. And whereas they would reach $K$ individuals before, they will now
reach $r/\alpha$. These are two worlds with very different emphasizes: the
first has an upper limit to growth, which is hard-coded. The second _also_ has
an upper limit to growth, but this time it emerges from the choice of being
explicit about the fact that individuals compete for the resource.

But of course neither of these models are explicit about the fact that resources
flow in and out of the system, and so we may want to add an equation for this.
And we would need to add a term to explain how the resource is converted into
biomass for the population. Should it be fixed, or depend on the metabolic
rates? There is a very deep rabbit hole we can bury ourselves in just when
deciding how to represent the simple fact that living organisms need to eat in
order to grow.

With this in mind, it is hardly surprising to create models whose behavior we
cannot anticipate. Once the protocol/model is setup, comes the exploration
phase, and the manipulation phase. In a way, speaking about &#8220;numerical
experiment&#8221; is not some sleight of hand designed to make modeling look
more practical than what it is; as a modeler, I _am_ experimenting on my system,
because although I may have created it, I do not understand it.
