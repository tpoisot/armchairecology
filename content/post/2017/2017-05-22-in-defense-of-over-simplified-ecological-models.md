---
title: In defense of over-simplified ecological models
date: 2017-05-22
slug: in-defense-of-over-simplified-ecological-models
author: tpoisot
categories: [Unquantified opinions]
topics: [ecological modelling]
---
There is a very important family of models in ecology based around describing the flows and fluxes of quantity across &#8220;boxes&#8221;. This can be biomass across species, alleles across spatial patches, population size, individuals across age classes, etc. Almost all of these models are based on ordinary differential equations, and they use parameters to express ecological processes. And the more quantities you want to model, the more parameters you need to link them together. As a result, complexity of the models often increase in a non-linear way with regard to the size of the problem.

<!--more-->

**And parameters are a problem**.

![UNTITLED IMAGE](/images/wp-content/uploads/2017/05/c4y_vrvwmaauxmzlarge.jpg)

I feel the need to preface this by highlighting that I have a lot of love and admiration for the model-based literature in ecology. Our lab space is decorated with some of the foundational equations of the field. But the issue with these models is that, the more parameters we need, the more complex the analysis is.

One of my favorite models (for teaching purposes) is the <a href="http://www.maa.org/press/periodicals/loci/joma/the-sir-model-for-spread-of-disease-the-differential-equation-model" target="_blank" rel="noopener noreferrer">SIR model of epidemiological dynamics</a>. It models three quantities (number of Susceptible, Infected, and Recovered individuals in a finite-size population at demographic equilibrium). It does so with two parameters: the rate of infection, and the rate of recovery (plus a sorta-kinda parameter, the initial number of Infected individuals). This model can be modified to allow Recovered individuals to lost their immunity (the SIRS ), with the addition of a single parameter. <a href="http://anintroductiontoinfectiousdiseasemodelling.com/" target="_blank" rel="noopener noreferrer">The book by Vynnycky & White</a> has a lot more illustrations of how epidemiological models work.

But not ecological models are so elegant. Let&#8217;s take a one-predator, one-prey model &#8211; _a.k.a._ <a href="https://en.wikipedia.org/wiki/Lotka%E2%80%93Volterra_equations" target="_blank" rel="noopener noreferrer">the Lotka-Voltera equations</a>. For two modeled quantities, there are four parameters. If you want to add a second prey, you will need to specify its growth rate (that&#8217;s one), consumption rate by the predator (that&#8217;s two), rate at which it is converted in predator biomass (three), and maybe something about its competition with the other prey (this can cost you between one and four parameters). How complex can a model about competition and predation become? Have a look at <a href="http://www.nature.com/nature/journal/v456/n7219/abs/nature07248.html" target="_blank" rel="noopener noreferrer">the great paper by Chesson & Kuang</a>, you should get a good idea (Box 1 kept me occupied for about a week).

Every parameter should, in theory, be evaluated against empirical data to provide a realistic value to plug in the equations (or the simulations). For example, the (equally great) paper by <a href="http://onlinelibrary.wiley.com/doi/10.1111/j.1461-0248.2006.00978.x/abstract" target="_blank" rel="noopener noreferrer">Brose and colleagues on allometric scaling in food webs</a>, uses published estimates of metabolic rates, and the output should therefore make ecological sense. After a year of working on this model with a student, we are still unsure about what sense it makes, exactly. Everyone&#8217;s best guess is that the output will be, in a hopefully linear way, proportional to &#8220;the real thing&#8221; (which may be equally difficult to measure, but this is a story for another time).

**And adding more and more parameters has consequences**.

Specifically, it has consequences about the way models are received by non-modelers. From my limited experience, non-modelers are never happy with the amount of parameters in a model. A small number (which make them slightly easier to calibrate) means the model is unrealistic; a large number means the model is hard to understand. Both of these statements, by the way, are correct. The question we should ask, is: what do we prefer?

As far as I am concerned, I would rather have a three-parameters model which I can calibrate against empirical data, than a twenty-parameters model that, although capturing all of the fine-scale mechanisms we care about, is practically impossible to reconcile with observations.

Still in my limited experience, the teaching of ecological modelling emphasizes the sort of models that include many parameters. This is important from a teaching perspective, because being able to work through something with a degree of complexity going up to multiple-species adaptive dynamics is providing a student with a lot of nifty calculus tools (or one can use _Mathematica_). But I am fond of toy models, with probably a little too few parameters &#8211; if it means that sacrificing some correctness allows me to more easily relate them to empirical data.
