---
title: Live mathematical modeling
date: 2017-12-04
slug: live-mathematical-modeling
author: tpoisot
layout: post
topics:
  - Modeling
  - Teaching
---

One of my more vivid memories of my graduate studies is a lecture on
evolutionary models. Not especially because of the content (something about
mutations, maybe?), but because of _how_ it was delivered. Instead of starting
with a question, and then showing the model, and then solving it, the beginning
of the lecture was &#8220;Here&#8217;s today&#8217;s topic, what question do you
want to explore?&#8221;. After a (short) brainstorming, we settled on a topic.

<!--more-->

This is when the fun started. As a class, we started discussing ways to represent the relevant mechanisms as equations, and then (using a blackboard), the lecturer assembled the model. This was a transformative moment for me (as I had no previous training in modeling): models are not things you pick from a giant library of models. They are things you build to study a specific question. Modeling is <a href="http://armchairecology.blog/2017/03/31/ecological-modeling-is-world-building/" target="_blank" rel="noopener">world building</a>, and this was the first time I realized this.

There are (I think) two ways to introduce modeling in ecology. The first is &#8220;Here is thing called the Lotka-Volterra equations for predator prey dynamics&#8221;, and discuss them. The second is this &#8220;live modeling&#8221; approach, where the question comes first, and we build the model around it. We may end up with one of the &#8220;canonical&#8221; models, or with something different. The second approach is very close to live coding, _i.e._ writing code as you go along, and thinking out loud about it, to help learners understand the process of _creating_ something.

This live modeling approach also explores constructing the model. While performing the actual mathematical analysis, or the simulations, can be difficult (and rewarding), these involve different skills than coming up with the right formulation of _everything_ in the model. Looking back at some of my modeling experiences, I figure I spent more time building the model, than solving it. In part, this is because we have numerical recipes to solve a lot of problems. But also, how to come up with the model is never really taught.

This winter, I will teach a new class (somewhat awkwardly named &#8220;_Computational biology & modeling_&#8220;), where we&#8217;ll explore the space between purely mathematical models and their implementation. A lot of this class will involve building toy models to play with, and I will use this live modeling approach as well as exploring some well established models. My feeling at the moment is that being able to build a model is a skill that is transferable to scientific activities that are not mathematical in nature: it forces one to think in terms of abstraction, to break down a problem in manageable components and deeply think about their behavior, and frame all of this in the &#8220;big picture&#8221;. This is likely more important than being able to remember the Routh–Hurwitz stability criteria.
