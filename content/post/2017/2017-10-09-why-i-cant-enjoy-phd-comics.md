---
title: "Why I can't enjoy PhD Comics"
subtitle: "And why I may be wrong"
date: 2017-10-09
author: tpoisot
layout: post
slug: why-i-cant-enjoy-phd-comics-and-why-i-may-be-wrong
comments: true
redirect_from: /2017/10/09/why-i-cant-enjoy-phd-comics-and-why-i-may-be-wrong/
---

Hey, before we start. I know. About grad school. I know it can make you miserable; this is when my first symptoms of [GAD](https://en.wikipedia.org/wiki/Generalized_anxiety_disorder) showed up. I had no adequate definition of work-life balance. I know there are a number of complex, related, systemic issues that can make grad school and the next years difficult. And then, there&#8217;s PhD Comics, and I don&#8217;t think it helps.

<!--more-->

**Sad is the new normal.**

Of course PhD Comics (you all know the link, by the way) is a _symptom_, and not a cause; it&#8217;s an exemplar of the idea that all graduates students _should_ feel awful all the time, and that&#8217;s the way it is. Knowing the <a href="http://www.sciencemag.org/careers/2017/04/phd-students-face-significant-mental-health-challenges" target="_blank" rel="noopener">prevalence of mental health issues among graduate students</a>, I am not sure that making light of the issue does any good. It makes it all too easy to dismiss legitimate distress as the normal state of a grad student. By establishing &#8220;absolute soul crushing misery&#8221; as the default, it becomes harder to decide when to seek help, and when to reach out to those around us that genuinely need help.

It also creates a new, insidious, version of impostor syndrome. I have had several encounters where grad students told me, in essence, &#8220;I feel fine, am I doing something wrong?&#8221;. No, they are not. The association between grad school and negative feelings is so deeply ingrained that it is easy to see how it can be taken for another metric of success.

**Or am I missing the point?**

Maybe. _Castigat ridendo mores_, was the motto of Italian poets: make people laugh as a way to make them think about their behaviors. But it only works when there is a suggestion of a way forward. And the 2nd best webcomic about grad school (the absolute best is [Colin Carlson&#8217;s #ASofterPhd](https://mobile.twitter.com/ColinJCarlson/status/908082983155900416), by the way) offers, in my opinion no such thing.

And the thing about satire, is that it comes at the issue from an interesting angle. [Jonathan Swift](http://art-bin.com/art/omodest.html) did not wrote &#8220;wow, you people sure are broke&#8221;; he wrote &#8220;we&#8217;re going to eat the poor&#8217;s babies&#8221;. And he did not wrote this for the poors &#8212; _A Modest Proposal_ was aimed at the rich, the educated, the people in power with the means to change the situation.

If we accept that mis-management, lack of work-life balance, and rampant mental health issues as the normal, there is little motivation to change things. None of these things are normal or acceptable. None of these things are particularly funny. It&#8217;s OK to mock them with a purpose. And I&#8217;m not sure this is always the case.

**But yet, we should be vocal about issues.**

Sharing grad school experiences is vital. Not only as a way to process them, but also because first generation university students have no notion of what grad school is. Because yes, grad school (as all of academia) will likely be a harsher experience for under represented minorities, and this requires strategies. Strategies to cope, and also strategies to effect change. It&#8217;s vital that these experiences are shared, and heard with respect. They deserve better than to be a laughing stock. In a sense, PhDComics may be a coping strategy. It may genuinely help some people; it never had this effect on me.

But with all of this attention paid to the &#8220;being miserable&#8221; part of grad school experience, we stopped talking about the things we need to talk about more. So here goes. [Grad students, you are extremely worthy and accomplished](http://mathbionerd.blogspot.ca/2016/02/you-are-worthy.html). Getting into grad school demands hard work, dedication, stubbornness, and sheer skill. You&#8217;re here because someone saw _something_ in you, and decided to help you nurture it. You may not know what this someone saw in you; this someone may not know it either. But it&#8217;s here. Your mental health matters.
