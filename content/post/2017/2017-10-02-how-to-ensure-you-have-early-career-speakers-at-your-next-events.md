---
id: 1409
title: How to ensure you have early career speakers at your next events.
date: 2017-10-02T08:30:07+00:00
author: tpoisot
layout: post
comments: true
guid: http://armchairecology.blog/?p=1409
redirect_from: /2017/10/02/how-to-ensure-you-have-early-career-speakers-at-your-next-events/
publicize_twitter_user:
  - tpoi
  - tpoi
publicize_google_plus_url:
  - https://plus.google.com/+TimothéePoisot/posts/jjcC3ZZgtqC
  - https://plus.google.com/+TimothéePoisot/posts/jjcC3ZZgtqC
image: img_20160906_1645322051273349.jpg
categories:
  - Unquantified opinions
topics:
  - conferences
  - diversity
  - early career scientists
---
An invited talk at a symposium or special session is an important milestone in a career. It shows that your work is recognized, to the point where others seek you out to talk about it. And when looking at the lineup, there is often a lack of early career guests. There are a series of steps organizers can follow, to make sure early career scientists are represented, and can be heard. I will walk you through them in a lot of details.

<!--more-->

**Step 1.** Invite early career scientists to speak at your event.

There is not step 2 (except of course &#8220;Check that you have a diverse list of early career speakers&#8221;). It is _literally_ that simple.

I have consistently invited early career (roughly meaning, not tenured) researchers at most events I had a part in organizing. This included special sessions, training events, and research workshops. Finding a list of people to invite has never been very difficult, because most of my early career fellows are actively looking for opportunities to talk. Though I may be biased as an organizer, these were all good events, with great and stimulating talks. The workshop, in particular, was very productive.

But let me appeal to the strongest emotion in academia: it&#8217;s good for your own reputation. What I found surprising, is that even though I never (except once) emphasized the presence of a majority of early career folks, _organizers noticed._ Invite junior colleagues, because it will get you some cookies points.

One of my former advisors had a more generous way of explaining the situation to me. As soon as people start grad school, they stop being students, and they become potential future colleagues. This bears two important consequences. First, people tend to remember how people higher up in the food chain treated them. Second, pay it forward as much as you can.

This is not to say that all events should have only early career speakers. Of course depth and breadth of expertise that come from a longer career, and more time to contemplate one&#8217;s field issues, are exceptionally important. But so is making sure that everyone is heard. Personally, this also have another upside: there is a whole lot of early career people out there, and it&#8217;s always fun to meet new people with new ideas.

And now, we have the full, step-by-step list
