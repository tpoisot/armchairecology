---
id: 1611
title: Fail, fail, fail. Then fail again.
date: 2017-11-27
slug: fail-fail-fail-then-fail-again
author: tpoisot
layout: post
guid: http://armchairecology.blog/?p=1611
redirect_from: /2017/11/27/fail-fail-fail-then-fail-again/
image: 20171124_095923.jpg
comments: true
categories:
  - Unquantified opinions
topics:
  - failure
  - impostor syndrome
---
Beckett said &#8220;Ever tried. Ever failed. No matter. Try Again. Fail again. Fail better.&#8221;. It is terse, bleak, and absolutely true. Especially when doing research, failure may be the most efficient way &#8211; as long as we fail in creative, novel, and interesting ways.

<!--more-->

![](/images//wp-content/uploads/2017/11/flat800x800075f.jpg)

Last week, we were lucky to host both Drs. [Auriel Fournier](https://aurielfournier.github.io/) and [Michiel Stock](https://michielstock.github.io/), and we had a group discussion about the qualities that are most helpful as a researcher. The first piece of advice was _Do not panic_. Specifically, do not panic when things apparently do not work; not working, when we do research, is the natural state of things. This is almost trivial to say, but **when things work as expected, it is not research anymore**. A lot of training in universities (at least the ones I attended) emphasize research as a linear process, like what you see in a lab class: you are given a protocol (that is 99% reliable), collect data (under a protocol we optimized to give a very clear result), then discuss these data under a pre-specified framework. This teaches valuable skills; but this does not teach you how to fail, or how to handle failure.

Looking back at my own papers, about 90% of them tell something different that what I originally thought they would. A minority of them turned out _really differently_. In fact, there are very few papers for which I followed a linear path from the original idea to the published product (<a href="http://armchairecology.blog/2017/09/18/hypotheses-are-over-rated-rewrite-them/" target="_blank" rel="noopener">this is why I am not opposed to HARKing</a>). And having 90% of the manuscripts take different directions does not mean that for every published paper, I threw nine away. I think I can only remember three manuscripts that I gave up on after having starting writing. Two of them ended up being merged into other projects. Even when the results take an unexpected direction, this is not a failure. It is an opportunity to write another story.

But most failure happens at a micro scale. And this is a good thing, because this is where you can use these failures to become better. The code is not working? I have found a way _not_ to solve this problem. The paper got sent for review for another round? I have found a way _not_ to write a convincing response to reviewers. The students struggled with an idea? I have found a way _not_ to communicate it. The model is not giving good results? I have found a way _not_ to represent this mechanism. **Having a library of examples of things that do not work is probably more important than having a library of examples of things that work perfectly**. I have found it very useful when triaging new problems: what are the previous ways I have failed to solve a similar problem in the past? Let&#8217;s not make these mistakes again.

In short, we should be more welcoming of a lot of times we &#8220;fail&#8221;. Failing is the expectation, when doing research. And so we should strive to fail in ways that are interesting. It is wise to remember <a href="https://en.wikipedia.org/wiki/Andy_Dwyer" target="_blank" rel="noopener">the words of Sir Andrew Maxwell Dwyer</a>: &#8220;I have no idea what I&#8217;m doing, but I know I&#8217;m doing it really really well&#8221;. I agree.
