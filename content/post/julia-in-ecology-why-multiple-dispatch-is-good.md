---
id: 1095
title: 'Julia in ecology: why multiple dispatch is good'
date: 2017-07-10T09:45:54+00:00
author: tpoisot
layout: post
comments: true
guid: http://armchairecology.blog/?p=1095
redirect_from: /2017/07/10/julia-in-ecology-why-multiple-dispatch-is-good/
image: 20170709_114207.jpg
categories: [Unquantified opinions]
topics:
  - programming
  - julia
---
In what is going to be the most technical note so far, I will try to reflect on a few years of using <a href="https://julialang.org/" target="_blank" rel="noopener">the Julia programming language</a> for computational ecology projects. In particular, I will discuss how multiple dispatch changed my life (for the better), and how it can be used to make ecological analyses streamlined. I will most likely add a few entries to this series during the fall, leading up to a class I will give in the winter.

<!--more-->

**But what is multiple dispatch**?

Imagine a recipe that calls for onions, and you have left in the cupboard is shallots. You know that shallots are little delicate bundles of gustative pleasure, and so you cook them differently (butter and half a teaspoon of sugar), extra gently. And when it&#8217;s done, you add them to the rest of the ingredients. This is multiple dispatch.

In computer terms now, we can express this  recipe as the following pseudocode:

~~~ julia
function cook(x::Onion)
   return fry(x, butter)
end

function cook(x::Shallot)
   return roast(x, butter, sugar)
end
~~~

If `x` is an onion, then we fry it. If it is a shallot, we roast it. The important point is that the _interface_ is the same: no matter what `x` is, we can `cook` it.

**And where is the ecology in that**?

Let&#8217;s talk about species interaction networks! One of the things that has been bugging me for a while was that we have no good, common interface to analyze them. There are a variety of packages that are either specific to some types of networks, or specific to some measures, or (worth) both. Because there are many different types of ecological networks.

Or are there? In <a href="http://poisotlab.io/EcologicalNetwork.jl/stable/types/" target="_blank" rel="noopener">EcologicalNetwork.jl</a>, I reduced them to a combination of two factors. Are they bipartite or unipartite, and are they quantitative, probabilistic, or deterministic.

In Julia, this can be explained by <a href="https://github.com/PoisotLab/EcologicalNetwork.jl/blob/master/src/types.jl" target="_blank" rel="noopener">a number of types and unions of types</a>, and this hierarchy allows to create a number of functions that have the same name, but behave in the correct way based on their input. For example, the number of species in a network is calculated differently if it is bipartite or unipartite:

~~~ julia
function richness(N::Bipartite)
   return sum(size(N.A))
end

function richness(N::Unipartite)
   return size(N.A, 1)
end
~~~

Where this becomes more interesting, is when we start chaining functions. For
example, we can take an empirical network, generate the probabilistic version
for a null model, then generate replicates, and finally measure the nestedness
on every replicate:

~~~ julia
using EcologicalNetwork
ollerton() |> null2 |> nullmodel .|> (x) -> nodf(x)[1]
~~~

This lines takes advantage of the fact that each function will take the &#8220;right&#8221; decision based on the type of its input. Specifically, it goes this way: the empirical network is a bipartite and deterministic one. The _null2_ function generates a probabilistic network which is also bipartite. This is passed to _nullmodel_, which will generate a number of bipartite deterministic networks, all of them are then passed through  the _nodf_ function to measure their nestedness.

And the resulting pipeline is also clear to read, and expresses what we _want_ to do (how we do it is determined based on the types). As a consequence, we can have a much more general package for network analysis.

**But why does this matter**?

Because, in short, it lets us (and yes, there are other paradigms that let us do the same thing) express _what_ we want to do. A good example would be measuring the diversity of an ecological community. Let&#8217;s say we have a site by species matrix, and this matrix has presence/absence data. We can measure diversity as the number of species as the sum of each row:

~~~ julia
function diversity(x::Array{Bool,2})
   return sum(A, 2)
end
~~~

But if we have quantitative information, then we may want to apply Pielou&#8217;s measure on each row instead:

~~~ julia
function diversity(x::Array{Number,2})
   return mapslices(pielou, x, 2)
end
~~~

In the case where we have a phylogenetic tree, then what about using PD?

~~~ julia
function diversity(x::Array{Number,2}, t::PhyloTree)
 return mapslices(n -> pd(n, t), x, 2)
end
~~~

And so on and so forth. In all of these situations, we know that the same concept (diversity) means different things as a function of the context &#8211; and for this reason, we want to do different things.

I like  this approach because it lets me focus on the _intent_ of what I want to do. The (still young) <a href="https://github.com/EcoJulia" target="_blank" rel="noopener">EcoJulia</a> project led by Michael Krabbe Borregaard is an attempt to use some of the niftiest features of Julia to develop general interfaces to some types of ecological data. This is something I am really excited to see happen.
