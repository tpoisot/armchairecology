---
title: Science Bioblitz, data quality, and crowd-sourcing
date: 2017-06-12T08:30:26+00:00
author: tpoisot
topics:
  - biodiversity
  - citizen science
  - eBird
  - iNaturalist
  - open data
---

Thanks to support from the <a href="http://www.cwf-fcf.org/en/" target="_blank" rel="noopener">Canadian Wildlife Federation</a>, the <a href="http://qcbs.ca/" target="_blank" rel="noopener">Québec Centre for Biodiversity Sciences</a>, and the <a href="http://www.faecum.qc.ca/" target="_blank" rel="noopener">Federation of Students Associations at the Université de Montréal</a>, we have organized a science bioblitz at the Laurentians biology field station, operated by the Université de Montréal. Now that <a href="https://www.inaturalist.org/projects/science-bioblitz-canada-150-station-de-biologie-des-laurentides" target="_blank" rel="noopener">a good fraction of the data are online</a>, I wanted to have a look at the results.

<!--more-->

First, **wow**.

We had close to 70 amazing volunteer experts, and they have recorded (so far) 1174 observations, for 341 species. To give a bit of context, there are 5850 observations in Québec on _iNaturalist_. What it means is that, within four days of work, we have contributed one in five observations to the total.

![UNTITLED IMAGE](/images/wp-content/uploads/2017/06/medium-1.jpeg)There are two reasons that made this possible. First, obviously, is hard work by the experts. The second, sadly, is <a href="http://armchairecology.blog/2017/06/05/we-cant-see-the-forest-for-the-bird/" target="_blank" rel="noopener">the severe lack of data</a> for some regions, which I discussed last week. We are at a point where _every_ data counts, and all collection efforts should be encouraged. There is some effort to make in advertising that _iNaturalist_ exists (it is orders of magnitude more active in the US), in addition to advocacy for researchers to deposit their occurrence data.

Second, **we got a really good taxonomic coverage**.

One of the challenge was to gather experts with sufficient breadth of expertise, to have a broad idea of the overall biodiversity. Looking at the results, we got 444 plant observations (160 species), 290 birds (63 species), 133 fungi (69 species), and 62 amphibians (14 species); everything else was below 50 observations &#8211; but the insect data are not here yet.

Finally, **generating research grade data is difficult**.

In _iNaturalist_, there are three levels of quality: casual (species name and occurrence), in need of id (one additional piece of evidence, such as sound or image), and finally research grade (validated by several community members at the _species_ level). Only the research grade data are sent to <a href="http://www.gbif.org/" target="_blank" rel="noopener">GBIF</a>. Of course, research grade is what we would prefer, but they are very difficult to achieve.

In practice, 217 plant observations currently quality as research grade (219 need identification), 45 for amphibians and fungi, and 36 for fishes. The lowest rate of research grade was for birds (almost no observations, because no pictures were taken).

There is an interesting general discussion to have on identification and expertise and quality. Relying on &#8220;auditable evidence&#8221;, _i.e._ every observation has one associated picture, means that validation can be done by the community, instead of having to rely entirely on the expert. But on more practical matters, some things are really difficult to take pictures of. _eBird_ data, for example, are uploaded on GBIF with virtually no check.

There is an interesting tradeoff to explore. When the data quantity is very low, any single point can have a very strong effect on the output on any analysis. In this situation, it is justifiable to sacrifice data volume for data quality. When the volume of data is large, there is (i) a lot of redundancy and (ii) a lower chance of a single data point having very strong effects. In a way, the way _iNaturalist_ and _eBird_ decide which data should be gifted to the research community are emblematic of two cultures: _eBird_ blindly trusts the expert, and _iNaturalist_ is betting on crowd-sourcing.

There is no telling which model is  the best one (my own inclinations go towards &#8220;trust no one, especially not yourself&#8221;). But when discussing the success of different initiatives by using the volume of data as a metric, we should always, _always_, keep in mind that there are different degrees of filtering applied.

&nbsp;
