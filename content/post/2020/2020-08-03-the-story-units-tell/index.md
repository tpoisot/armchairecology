---
title: "The stories units tell"
subtitle: "Grammage, fuel efficiency, and recovery time"
slug: stories-units
date: 2020-08-03
category: "Ecology & maths"
topics: [mathematics, epidemiology]
author: tpoisot
---

One of the ways to describe paper is its *grammage*, which is most commonly
expressed in gsm, *i.e.* grams per squared meters. Your usual notebook will be
somewhere around 60 gsm, a really heavy paper can be 120, a business card is up
to 400, and the standard copy paper is about 30. Higher grammage does not mean
that the paper will be "better" - this is simply an indication of how thick,
rigid, etc, the paper will be. But we are not here to talk about paper quality.

Expressing paper "density" as grams (a weight) per squared meters (a
surface) is an interesting choice, as it assumes that a sheet of paper is
a two-dimensional object. And this is an acceptable approximation, as your
average sheet of paper is thinner than a tenth of a millimeter. We *know* that
this object exists in three dimensions, but we are willing to *think of it*
as being two-dimensional. In short, our expression of grammage, in terms of
the units we picked, is technically incorrect, but it tells the correct story.

Another interesting unit is mpg, or miles per gallon, as a measure of fuel
efficiency; this is, quite literally, how many miles you will drive on a single
gallon of fuel. Because this is not necessarily intuitive, let's express this
instead as litres per kilometers, which amounts to a volume (a distance cubed)
divided by a distance - the resulting unit is therefore... a surface? How would
it make sense to measure the efficiency of a vehicle as a *surface*? Well,
it doesn't. We are perfectly justified in bringing all of these units to the
same basis, and the simplifying, but *this misses the point*. The point is
that a litre of fuel contains some quantity of energy, and the correct way
to think about miles per gallon is as an answer to the question: how much
energy do I need to expend to advance one unit of distance. Realizing that
a volume is a distance cubed is correct, but tells the wrong story.

**Now, what does it have to do with anything**?

One of the first exercises I assign in my modeling class is to think about ways
to come up with approximate values for the parameters of an epidemic model in
which individuals are initially susceptible, can become infectious, and finally
recover. The specific question I ask is, "can you think of a way to come up
with a good enough guess for the rate of recovery". This problem becomes
really simple if you think about units. In this model, we are describing
individuals, so the units of all variables are "individual". Because this is
a temporal model, the derivatives are expressed as changes in individuals,
or individuals per time unit. If we assume that the only way to recover is
to start as an infectious individual, then we know that we are multiplying
something which is expressed in individuals (the number of infectious),
and we must get something expressed as individuals per time.

Or to put it in not-words:

$$\text{individuals} \times u = \frac{\text{individuals}}{\text{time}}$$

From this, it is easy to see that the units of the quantity $u$ (*i.e.* the
unit of the recovery rate) is $\text{time}^{-1}$. This is a rate, and one way
to estimate a rate is to guess the time spent in the state, and then invert
it -- in this case, by guessing how long an individual remains infectious for,
we can express the rate of recovery as the inverse of this quantity.

Neat, right? This is an example where units can serve as a check that the
model parameters are correct (*everything* must balance at the end), and
also provide some guidance about what should be measured. Discussing units
(of variables and parameters) is one of my favorite ways to go through the
process of debugging a model, and it helps decide on the "correct" way to represent a mechanism.
