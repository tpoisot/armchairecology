---
author: "tpoisot"
draft: true
title: "Flux.jl for species distribution models"
slug: "flux-sdm"
date: 2020-07-31
topics:
subtitle: "(but mostly some other things)"
---


In this entry, I will very briefly show how we can use `Flux.jl` to train a
neural network on species occurrence data. This is mostly an exercise in also
featuring some of what the `GBIF.jl` and `SimpleSDMLayers.jl` are capable
of, as we have been making progress on these over the last few months. As
of this writing, a number of functions of both packages require the `master`
branch, and have not yet been released.

## Data collection

This first part will have *nothing* to do with `Flux` - instead this is a
pipeline to download occurrence data, download bioclimatic information, and
finally create a layer for pseudo-absences. In brief, this is a template that
may be useful to ecologists wanting to use these packages in their own work,
and a required component in what comes next.

### Getting occurrence data

As a first step, we will download occurrence data from *Iris versicolor*
in North America, using the `GBIF` package. This should not take very long,
as there are about 3000 human observations (we remove collected specimens).

````julia
using GBIF
iris = taxon("Iris versicolor")
````


````
GBIF taxon -- Iris versicolor
````





Getting the actual occurrences is done by generating an initial query,
and then completing it unless all records have been retrieved.

````julia
lat, lon = (34.0, 57.0), (-103.0, -46.0)
iris_data = occurrences(
    iris,
    "decimalLatitude" => lat,
    "decimalLongitude" => lon,
    "basisOfRecord" => "HUMAN_OBSERVATION",
    "limit" => 300
)
while size(iris_data) < length(iris_data)
    occurrences!(iris_data)
end
````





The `iris_data` variable has a list of the 2517 observations
we can use to build our model. So let's move on to the next step, which is
to get some predictors.

### Getting predictor data

We will use the 19 BioClim variables, and crop them to make sure that we
do not get more than the area we want. In the real world, we should be
doing some variable selection, and maybe add some landcover information,
but bioclim will do for now.

````julia
using SimpleSDMLayers
raw_predictors = worldclim(
    1:19,
    left=minimum(lon),
    right=maximum(lon),
    bottom=minimum(lat),
    top=maximum(lat)
)
````





As we will be doing some prediction using a neural network, it is a good
idea to process these layers a little bit - so we will be working on their
z-score instead.

````julia
using Statistics
means = mean.(raw_predictors)
stds = std.(raw_predictors)
function z_score(l::T) where {T <: SimpleSDMLayer}
    m, s = mean(l), std(l)
    f = (p) -> (p-m)/s
    return broadcast(f, l)
end
predictors = z_score.(raw_predictors)
````





We will keep the means and standard deviations stored because this will
allow us to transform other variables if we want to do some predictions
in a different area than the one used for training (which we will do, so,
heads up I guess).

### Generating pseudo-absences

One thing we do not have at the moment is a list of locations where the species
is *absent*. These data are usually not readily available, so we will make
do with a crude approximation, and generate some pseudo-absences. But first,
a bit of occurrences thinning.

To do this, we will limit the number of occurrences to one by grid cell:

````julia
presences_grid = Matrix{Union{Nothing,Bool}}(nothing, size(predictors[1]))
presences = SimpleSDMResponse(presences_grid, predictors[1])
for i in eachindex(presences_grid)
    if !isnothing(predictors[1][i])
        presences[i] = false
    end
end
for occ in iris_data
    presences[occ.longitude, occ.latitude] = true
end
````





We now have 226 cells with one occurrence. At this point,
we can create a list of pixels where we can draw pseudo-absences; we will
assume that pseudo-absences can be anywhere within a radius of 200km from
a cell in which we have an observation:

````julia
absences = similar(presences)
allpixels = vec([(lon, lat) for lat in latitudes(presences), lon in longitudes(presences)])
filter!(p -> !isnothing(presences[p...]), allpixels)
occupied = filter(p -> presences[p...] == true, allpixels)
radius = 200.0
for pixel in occupied
    d = [SimpleSDMLayers.haversine(pixel, p) for p in allpixels]
    in_range = findall(d .<= radius)
    for ir in in_range
        absences[allpixels[ir]...] = true
    end
end
````





This leaves us with 7850 potential pixels in which we can have
a pseudo-absence. And with this step done, we can move on to data preparation.

## Data preparation

### Building a dataset of pixels

As we have created *pseudo*-absences, we can cheat a little bit and use
the same number of absences and presences, for a prevalence of 0.5. We will
therefore pick a number of pixels in the `absences` layer, and all pixels in
the `presences` one. A pixel will be represented by its `(lon, lat)` tuple,
which will be used to extract the features (from the worldclim layers).

To avoid inconsistencies, we will *never* assign to an absence a pixel in which
we have a confirmed presence, so this requires an extra step of filtering.

````julia
using StatsBase
positives = vec([(lon, lat) for lat in latitudes(presences), lon in longitudes(presences)])
filter!(p -> presences[p...] == true, positives)
negatives = vec([(lon, lat) for lat in latitudes(absences), lon in longitudes(absences)])
filter!(p -> p in positives, negatives)
filter!(p -> absences[p...] == true, negatives)
negatives = sample(negatives, length(positives); replace=false)
````


````
226-element Array{Tuple{Float64,Float64},1}:
 (-75.41666666666667, 45.416666666666664)
 (-72.25, 45.25239808153477)
 (-75.41666666666667, 44.102517985611506)
 (-74.41666666666667, 40.48860911270983)
 (-90.91666666666667, 46.56654676258992)
 (-80.58333333333333, 43.445443645083934)
 (-92.41666666666667, 46.73081534772182)
 (-89.58333333333333, 46.23800959232614)
 (-91.25, 47.387889688249395)
 (-83.91666666666667, 46.40227817745803)
 ⋮
 (-81.91666666666667, 42.62410071942446)
 (-76.41666666666667, 44.59532374100719)
 (-84.58333333333333, 47.05935251798561)
 (-80.75, 46.56654676258992)
 (-77.25, 44.4310551558753)
 (-75.58333333333333, 45.58093525179856)
 (-73.91666666666667, 45.74520383693045)
 (-79.91666666666667, 43.281175059952034)
 (-72.58333333333333, 42.95263788968825)
````





### Preparing features

Of course, the coordinates of the points are *not* the features, so we will
now extract these:

