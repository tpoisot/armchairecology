---
title: "Non-dimensionalization of ecological models"
subtitle: "Sometimes less is more"
slug: nondimensionalization
author: tpoisot
date: 2023-02-18
topics:
  - models
  - differential equations
  - predation
  - units
---

In this post, I will go through some notes on how to make ecological models
non-dimensional, mostly because I have been preparing my lecture notes on this
topic and thought it might be useful to share it with the world. We will go
through a little overview of what dimensional and non-dimensional quantities
are, talk about units, and then show how the approach works with models of
increasing complexity.

## But first, some notation

I will try to stick to a series of simple notation conventions in the post, and
I need to introduce them here. State variables in the model (*e.g.* preys) are
in lowercase, like $x$. State variables in the non-dimensional model are the
*same* letter but uppercase, like $X$. The units of $x$ are noted as $[x]$, and
for a non-dimnensional quantity $n$, *i.e.* $n$ is expressed without reference
to a unit, $[n] = 1$.

Whenever the model requires additional parameters as *placeholder* parameters,
they will be noted as $h_m$, where $m$ is the index of this parameter. By
placeholder parameters, I mean any combination of model parameters that need to
be sorted out during the non-dimensionalisation process.

Finally, when we have *compound* parameters (*i.e.* the new parameters in the
non-dimensional models), I will note them however I think makes sense. In a few
cases, this will be $c_m$, but in other cases I will introduce notation that
looks like the original model.

## Units (and why they matter)

Remember when, at some point in school, you heard "we don't add apples and
oranges"? What this really means is, some operations only work when applied to
operands that share the same unit. For example, two meters and two meters can be
added (and give four meters), and multiplied, and they give four meters squared.
In other words, $[x+x] = [x]$, $[x + y] = [x]$ (but this is only allowed if $[x]
= [y]$), $[x^2] = [x]^2]$, $[x\times y] = [x][y]$, $[x / y] = [x][y]^{-1}$,
$1[x] = [x]$, and $[x/x] = 1$. **This is important**, because we will rely on
this a lot to identify our new compound parameters. Specifically, we cannot
introduce anything in the model that will break these relationships.

One more thing. Whenever we write an equations as $\text{lhs} = \text{rhs}$,
what this *also* implies is $[\text{lhs}] = [\text{rhs}]$. This is the one rule
that we *cannot* break, and the one that makes this whole edifice hold together.

## Will you get to the point?

Eventually.

## What is the point of non-dimensionalization?

Before I tell you this, let me introduce a very simple model of population
growth, with a growth rate $r$ and a carrying capacity $K$:

$$\frac{\text{d}}{\text{d}t}x = r\times x \times\left(1-\frac{x}{K}\right)$$

What are the units of this model?

One way to find out is to say, well, this is a derivative that represents the
change in a quantity over time, so the units are $[x][t]^{-1}$. This is correct,
and this can help us figure out the units of the other parameters:

$$[x][t]^{-1} = {\color{darkblue}[r][x]}\left({\color{purple}1-\frac{[x]}{[K]}}\right)$$

We know the what $[x]$ is (*e.g.* total biomass, number of individuals, etc.),
and we can use the rules about handling units to figure out the purple part.
Specifically, $[x]-[y]$ is only a valid operation when $[x]=[y]$, so we know
that

$$\color{purple}\frac{[x]}{[K]}=1$$

*i.e.* $[K] = [x]$. This makes sense, as this matches the definition of a
carrying capacity: the maximum "amount" of $x$ that the system can accomodate.
This means that we can now solve the blue part, namely

$${\color{blue}[r][x]} = [x][t]^{-1}$$

which turns out to be $[r] = [t]^{-1}$. Note that this *also* matches the
definition of a rate, as the inverse of a duration.

**And now the *point* of all this: let's get rid of these units**.

There are a few reasons to do it, but the main reason is, non-dimensional models
are almost always simpler, in that they involve fewer parameters. More
importantly, because removing units is also removing scale, we can compare the
dynamics of systems with different "rythms".

## So... how do we remove dimensions?

Great question, I'm glad you asked.

### Non-dimensional state variables

We will start by making our state variables dimensionless. Most models in
ecology are autonomous systems, in which the independent variable for
integration (usually time, in which case autonomous is usually called
'time-invariant', even though we very much model variation through time) does
not show up in the model. But we *also* need to make time non-dimensional!

So let's start with this step. In our logistic growth model, time is represented
by $t$, which has units $[t]$ (years, days, minutes, generations, ...). In order
to create a time $T$ that has no units (this makes sense I swear), we can use
the relationships about units:

$$t = T\times P_t$$

Here, $P_t$ is a *dimensional* scaling parameter with units $[t]$, so that $[T] = [t]/[P_t] =
1$: we have a non-dimensional way to represent time.

Defining $U = u\times P_u$ or $U = u\times P_u$ is mostly a matter of
convention, and only results in the scaling parameter being dimensional or not.
In the interest of figuring out what the value of the scaling parameter is for
an actual system, it might be a good idea to make it *dimensional*, so its
relevance to other state variables / parameters becomes clear.

By the same process, we have $x = X\times P_x$, with $[P_x] = [x]$, and $[X] =
1$.

### Setting up the derivative

The first step is going to replace $\text{d}x/\text{d}t$ by something involving
$X$ (the dimensionless state variable) and $T$ (the dimensionless time). If the
goal is to have a non-dimensional model, we know that our derivative is going to obey

$$[\text{whatever the derivative is}] = 1$$

And because we are interested in the change in $X$ over $T$, the derivative is
going to be

$$\frac{\text{d}X}{\text{d}T}$$

But remember that we *scaled* $x$ and $t$, by respectively $P_x$ and $P_t$, so the *actual* left-hand side of our model is

$$\frac{\text{d}X}{\text{d}T}\frac{P_x}{P_t}$$

It is literally that simple: every time we see $x$, we write $X\times P_x$, and
everytime we see $t$, we write $T\times P_t$. So we can rewrite our entire model
as

$$\frac{\text{d}X}{\text{d}T}\frac{P_x}{P_t} = r\times X \times P_x \times \left(1 - \frac{X\times P_x}{K}\right)$$

First things first, let's move $P_x/P_t$ to the right hand side and simplify, to
get

$$\frac{\text{d}}{\text{d}T}X = r\times P_t \times X \times \left(1 - \frac{X\times P_x}{K}\right)$$

### Identifying the new parameters

We can rewrite the model above as 

$$\frac{\text{d}}{\text{d}T}X = h_1 \times X \times \left(1 - h_2\times X\right)$$

with $h_1 = r\times P_t$, and $h_2 = P_x / K$. This is far more useful than it
looks, because now we can get values for $P_t$ and $P_x$, because we know $[h_1]
= [h_2] = 1$. The obvious solution here is to set $P_t = r^{-1}$, and $P_x = K$.
Note that the units still match, and that $h_1$ and $h_2$ are both
dimensionless.

In this simple example, we see that the parameters entirely disappear from the
model ($h_1 = r/r$, $h_2=K/K$), and the final model is

$$\frac{\text{d}}{\text{d}T}X = X \times \left(1 - X\right)$$

This model is describing the variation in the size of a population *scaled by
its carrying capacity*, over time *scaled by its growth rate*. If we want to
"project" the results back into the space of dimensional variables, then we need
to multiply the dimensionless time by $r^{-1}$, and the dimensionless population
size by $K$. Easy peasy.

## What about a more complicated model?

Sure.

Let's consider a predator-prey model with a Type II functional response with
handling time $\tau$, attack rate $\alpha$, eco-trophic efficiency
$\varepsilon$, and predator death rate $\mu$:

$$
\begin{aligned}
\frac{\text{d}}{\text{d}t}x & = rx\left(1-\frac{x}{K}\right)-\frac{\alpha x y}{1 + \alpha \tau x} \\\\
\frac{\text{d}}{\text{d}t}y & = \varepsilon\frac{\alpha x y}{1 + \alpha \tau x} - \mu y
\end{aligned}
$$

This model is a good candidate for non-dimensionalization, as it has a *lot* of
parameters, so we may want to remove a few of them. As we did before, let's
create non-dimensional state variables and their scaling coefficient:

$$
\begin{aligned}
t &= T\times P_t \\\\
x &= X\times P_x \\\\
y &= Y\times P_y \\\\
\end{aligned}
$$

This is enough to rewrite the model as

$$
\begin{aligned}
\frac{\text{d}}{\text{d}T}X\frac{P_x}{P_t} & = r\times X\times P_x\times\left(1-\frac{X\times P_x}{K}\right)-\frac{\alpha \times X\times P_x \times Y\times P_y}{1 + \alpha \tau \times X\times P_x} \\\\
\frac{\text{d}}{\text{d}T}Y\frac{P_y}{P_t} & = \varepsilon\frac{\alpha \times X\times P_x \times Y\times P_y}{1 + \alpha \tau \times X\times P_x} - \mu \times Y\times P_y
\end{aligned}
$$

As before, we can move the fractions of scaling parameters to the right, and
simplify. In order to make the next step easier, we will also start grouping
things are are state variables and things that are not, in order to identify our
placeholder parameters:

$$
\begin{aligned}
\frac{\text{d}}{\text{d}T}X & = r\times P_t\times X\times\left(1-X\times\frac{P_x}{K}\right)-\frac{\alpha \times P_y\times P_t\times X\times Y}{1 + \alpha \times \tau \times P_x\times X} \\\\
\frac{\text{d}}{\text{d}T}Y & = \varepsilon\times \frac{\alpha \times P_x\times P_t \times X \times Y}{1 + \alpha \times \tau\times P_x \times X} - \mu \times P_t \times Y
\end{aligned}
$$

What should our placeholder parameters be? Essentially, these would be *any
group of parameters acting on a state variable*, and more specifically, *from
which we can get an expression for the scaling parameters*.

Feb. 18, 2023 &mdash; Fixed a subscript mistake which led to the wrong
parameters; updated the discussion of the compound parameters.
{.edit}

Here, we can pick the following:

$$
\begin{aligned}
h_1 &= r\times P_t \\\\
h_2 &= P_x\times K^{-1} \\\\
h_3 &= \alpha \times P_y\times P_t \\\\
h_4 &= \alpha \times P_x\times P_t \\\\
h_5 &= \alpha \times \tau\times P_x \\\\
h_6 &= \mu \times P_t
\end{aligned}
$$

This means that our model is now

$$
\begin{aligned}
\frac{\text{d}}{\text{d}T}X & = h_1\times X\times\left(1-X\times h_2\right)-\frac{h_3\times X\times Y}{1 + h_5\times X} \\\\
\frac{\text{d}}{\text{d}T}Y & = \varepsilon\frac{h_4 \times X \times Y}{1 + h_5\times X} - h_6\times Y
\end{aligned}
$$

Getting back to the definitions of the placeholder parameters, it is now obvious
that we have multiple ways to define the scaling parameters! For example we can
use $h_1$ to say $P_t = r^{-1}$ (as we had in the logistic growth model).
Alternatively, maybe we are more interested in the demography of the predator,
and we want to use $h_2$ to have $P_t = \mu^{-1}$, in which case we would be
scaling time by the predator half life. Regardless of which one we chose, we
will set a value of $P_t$, and rewrite our placeholder parameters to see what we
can do. To keep things consistent with the previous model, we will use $h_1$ to
set $P_t = r^{-1}$, giving

$$
\begin{aligned}
h_1 &= 1 \\\\
h_2 &= P_x\times K^{-1} \\\\
h_3 &= \frac{\alpha}{r} \times P_y \\\\
h_4 &= \frac{\alpha}{r} \times P_x \\\\
h_5 &= \alpha \times \tau\times P_x \\\\
h_6 &= \frac{\mu}{r}
\end{aligned}
$$

We are making great progress, because we have either eliminated parameters
entirely ($h_1 = 1$), or expressed placeholder parameters as a functions of the
original parameters (like $h_6 = \mu/r$), which are ready to become compound
parameters in the non-dimensional model.

From here, the most obvious step is to set $P_x = K$, so $h_2 = 1$. This is an
obvious step because it means we are removing one more parameter from the
non-dimensional model, and we are left with

$$
\begin{aligned}
h_1 &= 1 \\\\
h_2 &= 1 \\\\
h_3 &= \frac{\alpha}{r} \times P_y \\\\
h_4 &= \frac{\alpha\times K}{r} \\\\
h_5 &= \alpha \times K\times \tau \\\\
h_6 &= \frac{\mu}{r}
\end{aligned}
$$

The only scaling parameter remaining is $P_y$, which we simply define as
$\frac{r}{\alpha}$, to have

$$
\begin{aligned}
h_1 &= 1 \\\\
h_2 &= 1 \\\\
h_3 &= 1 \\\\
h_4 &= \frac{\alpha\times K}{r} \\\\
h_5 &= \alpha \times K\times \tau \\\\
h_6 &= \frac{\mu}{r}
\end{aligned}
$$

Out of these six placeholder parameters, picking the right values for the
scaling parameters made three disappear. The last three cannot be reduced
further, but we can make them into *compound* parameters, $p_1 = h_4 = \alpha
K/r$, $p_2 = h_5 = \alpha K \tau$, and $p_3 = h_6 = \mu/r$.

This gives us the following non-dimensional model:

$$
\begin{aligned}
\frac{\text{d}}{\text{d}T}X & = X\times\left(1-X\right)-\frac{X\times Y}{1 + p_2\times X} \\\\
\frac{\text{d}}{\text{d}T}Y & = \varepsilon\times p_1\times \frac{X \times Y}{1 + p_2\times X} - p_3\times Y
\end{aligned}
$$

And *this* is a much more tractable version of the model. At this point, you
might be wondering why $\varepsilon$ is still there. It would be fine to merge
it with $p_1$, in which case this would be $p_1 = \varepsilon \alpha K r^{-1}$,
but there are two reasons not to do it. First, this parameter is *already*
dimensionless. Second, it looks like it's important, and so it might be a good
idea to express the solutions, *e.g.* the equilibrium, or stability conditions,
as a function of it. Functionally, the dimensionless eco-trophic efficiency is
given by $\varepsilon p_1$, which (because the model is dimensionless) may not
always be lower than 1.

It is a good idea to make sense that our new parameters still make sense in
context. For example, $p_3$ is the dimensionless rate of decay of the predator,
and is expressed in the original parameters as $\mu/r$; which is to say, the
speed at which predators decay scaled for the time of our model. That tracks.
Similarly, $p_2$ is the rate of predator saturation in our now dimensionless
Holling Type II response, and it is expressed as $\alpha \tau K$; quite simply,
this is the time spent handling a population of size $K$ which is attacked at
rate $\alpha$. Interestingly, note that our "time" scale ($r$) is *not* showing
up in $p_2$. We should have been expecting this, in a way, as the saturation
process is purely based on how large $X$ ($x$) is.

We can finish by looking at our scaling parameters:

$$
\begin{aligned}
P_t &= r^{-1} \\\\
P_x &= K \\\\
P_y &= r\times \alpha^{-1} \\\\
\end{aligned}
$$

Time is scaled by the inverse of the prey growth rate, the prey population is
scaled by its carrying capacity and the predator population is scaled by the
prey growth rate divided by the attack rate.

## When should we use non-dimensional models?

Outside of the obvious answer (it is saturday and you feel like writing about
non-dimensional models), non-dimensional models can be helpful when you want to
remove scaling effects from your models. Because we have fewer parameters (for
the predation model, we went from six parameters to three!), the model is easier
to simulate, and the mathematical analysis of it is, similarly, simpler. In
cases where you need to work with analytical expressions of the equilibrium, for
example in adaptive dynamics, this can be a game changer.

An important feature of these models is that we can re-dimensionalize them. For
example, the equilibrium of the non-dimensional prey alone is $(X^\star,
Y^\star) = (1, 0)$, but because we know that this equilibrium in the original
model is $(x^\star, y^\star) = (P_x\times X^\star, P_y\times Y^\star) = (K, 0)$.
In a sense, these non-dimensional solutions are much more general than the
models with the full parameters!