---
title: The value of getting nothing done.
author: tpoisot
date: 2018-01-22
slug: getting-nothing-done
topics:
  - productivity
---

This week, I got nothing done. It was a good week. In fact, it was one of the
most productive weeks I had in a while. Let me explain. I did submit a paper,
and sent back some comments to co-authors on two manuscripts, attended a few
meetings and calls, and gave two lectures. But I did not make any of my own
projects move forward. And I did it on purpose.

<!--more-->

Instead on working on the things that needed work, I spent the week reading
papers, book chapters, and working on problems that were not immediately
relevant to any particular active project. I spent some time playing with
adaptive dynamics, some time working on machine learning methods I had not
explored yet, and tried to apply those I had previously explored to new
problems.

One of my colleague is often saying that we should take the time to just sit
back and think more often, and I decided to give this a try. So, in what way was
this productive? **I learned a ton of new things**.

In part, this was because I had no clear goal in mind. There was not this
self-imposed pressure of getting the job done, because nothing I did was tied to
a project. I was free to keep pushing when it was interesting, and free to drop
an idea when it went nowhere or started to be boring. And I was not ashamed in
getting rid of boring things, because discovery and exploration should be fun
(sometimes).

This was not entirely an exercise in indolence. All of the things I toyed with
were on my ever-growing list of "things I am curious about but never had the
time to think deeply about". I started the week with a rough schedule of things
to do, and worked on one, each day.

At the end of the week, I had a much better idea of the value of each of these
ideas / tools / concepts for my current and future projects. This is why I think
this week in which I got nothing done was, in fact, very productive. Because it
will pay off in the future, in new ideas or in new ways to explore old ones. And
I am quite sure I will do it again.

Maybe not next week.
