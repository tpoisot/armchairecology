---
title: When are networks worth it?
date: 2018-01-29
slug: when-are-networks-worth-it
author: tpoisot
uses: [cite]
topics: [ecological networks, information theory]
bibFile: "bib.json"
---

Last week, during a seminar, my colleague [Jesse Shapiro][jesse] asked a very
interesting question: are there some problems for which networks are not "worth
it"? Even though my knee-jerk answer was quite obviously "Nah", the actual
answer is far from trivial. So here are a few ways of thinking about it, and
absolutely nothing in the way of a solution

[jesse]: http://www.shapirolab.ca/

<!--more-->

The question can be re-framed a little bit -- when is it not worth paying the
cost that comes with the complexity of dealing with networks? As compared to
non-network based analyses, anything that involves networks has a narrower range
of measures, requires some more intensive computations to get the answer, and is
basically a different way of thinking about objects. So it makes a lot of sense
to ask if the increase in explanatory or predictive power justifies adding what
are essentially new terms or parameters in our model.

There are two situations, I would argue, where networks will not tell you
anything interesting. The first is when you have no interactions, or very close
to no interactions. This sounds trivial, but networks are a way of leveraging
information about the structure of interactions. Maybe there is a critical graph
density below which they don't really deliver. The second situation is, of
course, the opposite. If the network is entirely connected, or close to, then
you may as well remove the interactions (unless the strength of interactions is
quantified).

So my naive expectation is that networks should be really informative when you
have enough interaction to pick up some signal, but not so much that every node
is the same. If we play around with the idea of applying Shannon's entropy to a
network, where our two classes are no interaction, and presence of an
interaction, it is quite easy to see that the value would peak at density $\rho =
0.5$, *i.e.* half of the matrix representing our network is filled. We
previously found that this density maximizes the variance in the degree
distribution *and* the number of possible network arrangements {{< cite "Poisot2014When" >}}.

But this is sort of an useless answer, as most (ecological) networks are much
less connected than this, and this density is associated with lowest stability
in simulated networks {{< cite "Rozdilsky2001Complexity" >}}, which sounds like
a reasonable point to make. The point is, there is no clear cut answer to the
question of "How do we know if a network is worth it?". And reading some recent
papers, it is clear that we have a lot of arguments about *what networks can
deliver*, and *how to apply measures to extract information*, but virtually
nothing (that I know of) on *when* networks should or must be used. This is
obviously something to revisit more seriously.
