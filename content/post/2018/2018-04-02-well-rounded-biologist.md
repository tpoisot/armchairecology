---
title: The well-rounded biologist.
author: tpoisot
date: 2018-04-02
slug: well-rounded-biologist
topics:
  - academia
  - teaching
---

Earlier this week, students in the department organized our annual symposium.
During a panel, we came to discuss the skills of "the average biologist", and
what it means for the students currently entering the field. This was an
interesting discussion, and it made me think a little bit more about three
points: our expectations in terms of skills, inadequate training, and the need
to value different profiles.

<!--more-->

**We expect biologists to know how to do more and more things**.

There is a very clear message that in addition to be good a biologist (whatever
that means), you should know programming, data analysis, communication,
outreach, field work, lab work, some more data analysis, by the end of your
graduate studies. This is not possible. And sending this message to students is
exhausting for them.

The message we *should* send, instead, is that all of these skills are necessary
to the advancement of research, but nothing says that a single person should be
responsible for them all. There is a myth that says that you become a master at
something after ten thousand hours of practice. The estimate may be wildly off,
but there is truth in the idea that sustained practice over a long time is
requirement for mastery. To know something deeply takes time, practice, and the
conscious decision to not become an expert at most things.

Requiring biologists to know it all also sends the message that we should sell
ourselves as a master of many things. The truth is, probably very few people can
become experts at all of the aspects of science. After all this time, there is
about one and a half thing about which I would say I am an expert, and that is
*still* a preposterously generous estimate. But this is fine -- expertise in a
handful of things (a small hand, like you may find on a two or three toed sloth)
coupled with an awareness of other things, and a steadfast refusal to care about
a lot of other things, makes me productive.

**The training is lagging behind**.

Training currently increases in volume, because it seems to proceed from the
idea that there is a common core of knowledge for biologists, and this core is
huge. It varies across institutions, but we expect undergraduates to know *a
ton* of systematics and taxonomy, natural history, statistics, basic lab work,
genetics, and more specialized courses. So in order to keep up with the trend of
biology relying on an increasing number of skills, the trend seems to increase
the volume of content instead of making *choices*.

And I am all in favour of making cuts to the "common" set of courses. In fact, I
would probably be happy with the common core being a single class. It would be
pass/fail, and last for about 5 minutes. It would have a single multiple-choice
question: "Do you want to study biology? yes; no; unsure". Check "yes" or
"unsure", congratulations on being a biologist, let's pick you some classes to
get you where you want to be.

This would offer the huge opportunity of letting students grow into the
biologist they want to become. Want to spend 90% in the field generating data?
Go do it, we need this. Want to stay inside, drink tea, and do statistics?
Superb, that's useful. Want to do a mix of this, and help other people talk to
one another? Congratulations, this is a much needed profile.

**We need to value generalists and outfielders**.

Currently, we tend to value the *appearance* of being able to do it all. My
perspective on this is necessarily biased because I am not a very good
"classical biologist" (don't know much natural history, no interest for
taxonomy, would rather not go in the field, thinks the lab smells weird) despite
doing actual research in biology. But we need to improve the diversity of
profiles. The idea of the "average biologist" is interesting, and maybe we can
spend some time thinking about it. But I think it would be more interesting to
think about the range of *variance* around this idea we are comfortable with.
