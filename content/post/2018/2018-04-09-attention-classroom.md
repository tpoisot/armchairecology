---
title: I am not competing against laptops when I teach.
author: tpoisot
date: 2018-04-09
slug: attention-classroom
topics:
  - technology
  - teaching
---

Yesterday, I attended a workshop on principles for efficient teaching, organized
by the [Center for University Pedagogy][cpu] at the Université de Montréal. As
expected, one of the topics we adressed during the informal discussions was the
use of laptops in the classroom by students. In particular, one of the notions
we discussed was the idea of competition for the student's attention, in a
context where things they can do on their laptops provide more instant
gratification than hearing me prattle about the curse of dimensionality.

[cpu]: http://cpu.umontreal.ca/

<!--more-->

But when I was thinking about this on the way home, I realized two things.
First, as educators, we should only comment about this *if* our own practices
were above reproach; second, the notion that we are competing with facebook,
snapchat, or youtube (because yes, I've seen a student watch youtube during a
class) is probably flawed because it puts the fault on the students.

> **Update**: Before we start, I would like to point out that laptops are *required* for
> many students in order to learn. This is the reason for which I do not ban
> them in my classes. You can read more about these points in texts by [Godden &
> Womack][gandw] or [Cortland][cortland].

[gandw]: http://hybridpedagogy.org/making-disability-part-of-the-conversation/
[cortland]: https://eic.rsc.org/endpoint/why-banning-laptops-is-harmful/3008076.article

To begin with, **let's not be hypocritical about our own use of laptops and
cellphones during knowledge exchange scenarios**. During the average conference,
we spend a lot of time on email, finishing up our talk, reading papers, or just
generally not paying much attention to the speaker. In my case, the reason is
quite clear: not everything in ecology interests me. If you add, on top of that,
mediocre presentation skills, I probably spend more time *not* engaged, rather
than engaged, in talks.

Attention is a resource, and it is perfectly fine to manage it by not allocating
much of it to things that are not particuarly interesting to us (as long as we
are generally aware of the existince of these things). To push this reflexion a
bit further, if I as a communicator or educator think something should be
interesting to *everyone*, it is *my* responsibility to convey this. The fact
that a thing interests *me*, does not makes this thing interesting for all.

Coming to this realization over the last few years was liberating. I do not feel
offended when someone is obviously looking at their laptop if I am giving a
presentation. I have made my case about why I think my message is important and
relevant, and then the decision of paying attention or not is not in my hands
anymore. If I want more people to listen, I need to do a better job at
communication.

Which brings me to my second point: **I am not competing against the myriad of
possible distractions**. Students (or colleagues) turn to these distractions
because they answer a need that my own communication doesn't. What leads
students to browse facebook instead of paying attention is not the existence of
facebook, or the presence of a laptop on their desk, or the fact that they have
the app installed on their phone, or that we don't strap them to chairs *A
Clockwork Orange style; it's that our communication is not efficient, adapted,
or engaging enough. I'm not competing against these distractions, I am competing
against a version of myself which is less efficient at teaching.

As long as this is not causing disruption, I don't think less of students
chosing to do something else than pay 100% attention to me for three consecutive
hours (and by the way, this always struck me as an unrealistic expectation).
When I see students doing something else (and we can tell; by the way the eyes
move on the screen, the rythm of keystrokes, the facial expressions; we can
*always* tell), it is feedback that I need to change something.

I think **paying attention is about making choices**. About picking what we
think will matter, and therefore deserve an investment of our time, versus what
is a bit too remote to our own interests to be entirely engaged in it. After I
have made a case about what I believe is important, and why, students can make
their own choice (especially since I mostly teach to seniors and
post-graduates); this is the exact same logic I apply when deciding whether I
listen carefully to a talk or whether I do some edits on a manuscript. Maybe
they will get it wrong (and pay a price during the final exam, or in their
professional live), but maybe they will make the right call. As long as there is
no disruption to the students willing to pay attention, and as long as I have
done my best possible case to explain why I think they should pay attention,
this is out of my hands.
