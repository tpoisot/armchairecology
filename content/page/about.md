---
title: About
---

*Armchair Ecology* is a blog about the intersections between ecology, advanced
research computing, and mathematics. It is edited by [Timothée Poisot][tp].

The content of the blog is released under the [Creative Commons BY-4.0][ccby4]
license. This includes all images, except when specified otherwise at the bottom
of the post. Code snippets are considered part of the public domain and provided
without warranty of any kind.

Analytics for this website use [plausible.io][plausible], which is a
privacy-focused analytics system that does not collect personal data or
personally identifiable information, and does not rely on the use of cookies.

If you like what you're reading, you can contribute to the lab's coffee and
snack (and markers) fund [here][kofi]!

The source code for this blog is available on [GitLab][gl]. It is rendered using
[Hugo][hugo] on [Netlify][netlify]. Equations are rendered by [KaTeX][katex].
Comments are handled through [GraphComment][graphcomment].

Icons are provided by [remix icon][remixicon]. The text is set in Barlow, Barlow
Semi Condensed, and Barlow Condensed.

[gl]: https://gitlab.com/tpoisot/armchairecology
[hugo]: https://gohugo.io/
[netlify]: https://www.netlify.com/
[ccby4]: https://creativecommons.org/licenses/by/4.0/legalcode
[tp]: http://poisotlab.io/
[kofi]: https://ko-fi.com/timotheepoisot
[remixicon]: https://remixicon.com/
[katex]: https://katex.org/
[graphcomment]: https://graphcomment.com/
[plausible]: https://plausible.io