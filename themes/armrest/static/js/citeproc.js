var regexp = /(\[?)(@)(\w+\d{2}\w?)(\]?)/g;

var refs;
jQuery.ajaxSetup({async:false});
gref = function(d) {refs = d; return refs;}
$.getJSON("/references.json", gref);
jQuery.ajaxSetup({async:true});

var rdict = {};
for (var i = 0; i < refs.length; i++) {
	rdict[refs[i].id] = refs[i];
}

var p = d3.select("article").selectAll("p");
p.html(function(d) { return replacestr(d, this) });

function replacestr(d, p) {
	var rawtext = p.innerHTML;
	match = regexp.exec(rawtext);
	if (match != null) {
		var matchedref = match[3];
	  var brackets = match[1] == "[";
		var ref = rdict[matchedref];
		var fref = formatReference(ref, brackets);
		rawtext = rawtext.replace(match[0], fref);
		match = regexp.exec(rawtext);
	}
	return rawtext;
}

function formatReference (r, b) {
	// Prepare the link
	var link_title = r.title;
	var link_url = "#";
	if (r.URL) {link_url = r.URL};
	if (r.DOI) {link_url = "http://dx.doi.org/".concat(r.DOI)};
	// Deal with brackets
	if (b) {
		inline_text = `(${formatAuthors(r)}, <a href="${link_url}" title="${r.title}" target="_blank">${formatYear(r)}</a>)`;
	} else {
		inline_text = `${formatAuthors(r)} (<a href="${link_url}" title="${r.title}" target="_blank">${formatYear(r)}</a>)`;
	}
	// Return everything
	return `<span class='inline-bib ${r.type}'>${inline_text}</span>`;
}

function formatAuthors (r) {
	var number_of_authors = r.author.length;
	switch (number_of_authors) {
		case 1:
				return r.author[0].family
			break;
		case 2:
				return r.author[0].family.concat(" & ").concat(r.author[1].family);
			break;
		default:
		return r.author[0].family.concat(" <em>et al.</em>");
	}
}

function formatYear (r) {
	return r.issued["date-parts"][0][0];
}
