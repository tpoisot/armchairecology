Array.prototype.uav = function()
{
	var n = {},r=[];
	for(var i = 0; i < this.length; i++)
	{
		if (!n[this[i]])
		{
			n[this[i]] = true;
			r.push(this[i]);
		}
	}
	return r;
}

var regexp = /(\[?)(@)(\w+\d{2}\w?)(\]?)/g;

var refs;
jQuery.ajaxSetup({async:false});
gref = function(d) {refs = d; return refs;}
$.getJSON("/references.json", gref);
jQuery.ajaxSetup({async:true});

match = regexp.exec($('article').text());
while (match != null) {
  var matchedref = match[3];
  var brackets = match[1] == "[";
  var ref = refs.filter(function(a){return a['id'] == matchedref})[0];

  var inline = "";
  inline += ref['author'][0]['family'];
  if (ref['author'].length == 2) {
    inline += " & ";
    inline += ref['author'][1]['family'];
  }
  if (ref['author'].length > 2) {
    inline += " et al.";
  }

  var GOTO = '';
  if (ref['DOI']) {
    var GOTO = "http://dx.doi.org/"+ref['DOI'];
  } else {
    if (ref['URL']) {
      var GOTO = ref['URL'];
    } else {
      var GOTO = "#";
    }
  }

  var pubyear = ref['issued']['date-parts'][0][0];
  pubyear = "<a href='" + GOTO + "' title='" + ref['title'] + "' target='_blank' class='reference'>" + pubyear + "</a>";
  if (brackets) {
    pubyear = ", " + pubyear;
  } else {
    pubyear = " (" + pubyear + ")";
  }

  inline = inline + pubyear;

  if (brackets) {
    inline = "(" + inline + ")";
  }

	inline = "<span class='reference'>" + inline + "</span>";

  var new_html = $('article').html().replace(match[0], inline);
  $('article').html(function() {return new_html});

  match = regexp.exec($('article').text());
}
