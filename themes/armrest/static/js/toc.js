$(document).ready(function () {
    var headers = $("h2,h3").filter('[id]');
    var tocdiv = $('<div id="toc"></div>');
    if (headers.length > 0) {
        tocdiv.insertAfter("article p:eq(0)");
    } 
    for (i=0; i < headers.length; i++) {
        $('<a>',{
            text: headers[i].innerText,
            class: `toc-${$(headers[i]).prop("tagName")}`,
            href: `#${headers[i].id}`
        }).appendTo(tocdiv);
        console.log(headers[i]);
        if ($(headers[i]).prop("tagName") == "H2") {
            $('<a>', {
                text: "#",
                class: `margin-anchor margin-anchor-${$(headers[i]).prop("tagName")}`,
                href: `#${headers[i].id}`
            }).prependTo(headers[i]);
        } else {
            $('<a>', {
                text: "##",
                class: `margin-anchor margin-anchor-${$(headers[i]).prop("tagName")}`,
                href: `#${headers[i].id}`
            }).prependTo(headers[i]);
        }
    }
});
