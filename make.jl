using Pkg: Pkg
Pkg.activate(".")

root = pwd()

using Weave
using Literate

const content_folder = joinpath(pwd(), "content", "post", "drafts")

literate_config = Dict(
    "credit" => false, "execute" => true, "flavor" => Literate.CommonMarkFlavor()
)

post_folders = filter(isdir, joinpath.(content_folder, readdir(content_folder)))
for current_post_folder in post_folders
    try
        if "index.Jmd" in readdir(current_post_folder)
            @info "Index found"
            source_file = joinpath(current_post_folder, "index.Jmd")
            target_file = replace(source_file, "index.Jmd" => "index.md")
            if mtime(source_file) > mtime(target_file)
                @info "Weaving"
                cd(current_post_folder)
                Pkg.activate(".")
                post_name = splitpath(current_post_folder)[end]
                weave(
                    source_file;
                    out_path=target_file,
                    doctype="github",
                    # doctype = "hugo",
                    # fig_path = joinpath(post_name, "figures")
                )
                cd(root)
            else
                @info "$(target_file) is up to date"
            end
        end
        if "index.jl" in readdir(current_post_folder)
            @info "Index (julia file) found"
            source_file = joinpath(current_post_folder, "index.jl")
            target_file = replace(source_file, "index.jl" => "index.md")
            if mtime(source_file) > mtime(target_file)
                @info "Literating"
                cd(current_post_folder)
                Pkg.activate(".")
                post_name = splitpath(current_post_folder)[end]
                Literate.markdown(source_file, current_post_folder; config=literate_config)
                cd(root)
            else
                @info "$(target_file) is up to date"
            end
        end
    catch e
        print(e)
    finally
        cd(root)
    end
end
